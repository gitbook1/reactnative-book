# RN視覺類型資源

目前單頁或功能導向App應該都會直接先用`react-native-elements`為主，而在重構prtv3過程確認`react-native-elements`與`native-base`是可以混用，`native-base`算是一套完整的theme。

### 版型

1. [react-native-elements](https://react-native-training.github.io/react-native-elements/)：感覺算是RN的Bootstrap，提供基本但又有基礎樣式配置效果的元件，目前到1.1.0版
2. [NativeBase](https://docs.nativebase.io)：市佔率似乎頗高的RN UI套件，包含Market機制可提供原型版型付費購買機制。
   1. [官方元件說明](https://nativebase.io/)
   2. [An example app with all the UI components of NativeBase](https://github.com/GeekyAnts/NativeBase-KitchenSink)
   3. 使用NativeBase為基礎製作的UI版型套件(需付費)[Native Starter Pro Apps & Components](https://market.nativebase.io/view/native-starter-pro)
3. [react-native-elements](https://react-native-training.github.io/react-native-elements/)：感覺算是RN的Bootstrap，提供基本但又有基礎樣式配置效果的元件，目前到1.1.0版
4. [ui-kitten](https://akveo.github.io/react-native-ui-kitten/#/home)：Mobile framework with easily customizable elements，有包含以下元件，並且使用React-Navigation。
   1. RkCalendar(日曆日期選擇器可範圍)
   2. RkAvoidKeyboard
   3. RkModalImg(全螢幕展示)
   4. RkGallery(相簿元件)
   5. RkTabView(頁籤切換)
   6. RkSwitch(開關元件)
5. shoutem：本身是一個開發雲平台服務(類似Expo)但可獨立使用他的theme/ui等元件
   1. shoutem-ui：程式內匯入後可直接使用`<Example />`來展示所有可用的元件，詳細[文件參考](https://shoutem.github.io/docs/ui-toolkit/introduction)
   2. [shoutem-theme](https://github.com/shoutem/theme/tree/release/0.11.0)：使用react-native開發時可以參考套入，不過stable版本約2年前(2017)了。


### 表單類元件

1. [moschan/react-native-simple-radio-button: Simple and handy animated radio button component for React Native](https://github.com/moschan/react-native-simple-radio-button)：有應用在prt專案內，與下列textarea組合新元件。
2. [React Native auto growing text input](https://medium.com/@manojsinghnegi/react-native-auto-growing-text-input-8638ac0931c8)：textarea欄位，有應用在prt專案內。
3. [How to make your React Native app respond gracefully when the keyboard pops up](https://medium.freecodecamp.org/how-to-make-your-react-native-app-respond-gracefully-when-the-keyboard-pops-up-7442c1535580)：實際應用`KeyboardAwareScrollView`範例，處理鍵盤展開時避免遮蔽有效解法，實測發現android平台會失效！
4. [FaridSafi/react-native-gifted-form: 📝 « One React-Native form component to rule them all »](https://github.com/FaridSafi/react-native-gifted-form)：表單驗證元件以及多種操作欄位。


### 表格資訊

1. [react-native-data-table](https://www.npmjs.com/package/react-native-data-table)
![顯示效果](../assets/react-native-data-table.png)


### 操作功能

1. [swipeable views](https://github.com/oliviertassinari/react-swipeable-views)：直接手指左右滑動全螢幕切換
2. [react-native-side-menu](https://github.com/react-native-community/react-native-side-menu)：概念是主要以外面一個<View>包覆SideMenu以及內容置放的View作法
3. [lazaronixon/react-native-qrcode-reader](https://github.com/lazaronixon/react-native-qrcode-reader)：實作畫面掃二維碼範例專案
4. [johanneslumpe/react-native-gesture-recognizers: Gesture recognizer decorators for react-native](https://github.com/johanneslumpe/react-native-gesture-recognizers)：觸控操作偵測元件


### 多媒體處理元件
1. [ladas-larry/react-native-responsive-image: Responsive image component for React Native](https://github.com/ladas-larry/react-native-responsive-image)：響應式圖檔元件
2. [react-native-community/react-native-blur: React Native Blur component](https://github.com/react-native-community/react-native-blur)：顯示類似毛玻璃視覺效果
3. [leecade/react-native-swiper: The best Swiper component for React Native.](https://github.com/leecade/react-native-swiper)：圖片輪播效果
4. [phil-r/react-native-looped-carousel: Looped carousel for React Native](https://github.com/phil-r/react-native-looped-carousel)：進入App引導頁效果
5. [react-native-community/react-native-image-picker: A React Native module that allows you to use native UI to select media from the device library or directly from the camera.](https://github.com/react-native-community/react-native-image-picker)：裝置多媒體檔案選擇套件，包含可觸發照相、錄影等功能。
6. [ivpusic/react-native-image-crop-picker: iOS/Android image picker with support for camera, video, configurable compression, multiple images and cropping](https://github.com/ivpusic/react-native-image-crop-picker)：支援圖片多選、圖片裁切後處理功能套件。
7. [ascoders/react-native-image-viewer: 🚀 tiny & fast lib for react native image viewer pan and zoom](https://github.com/ascoders/react-native-image-viewer)：圖片檢視器可輪播檢視、放大縮小、手勢往下返回等操作。


### 日期選擇器
1. [henninghall/react-native-date-picker: React Native Date Picker - A new datepicker for Android and iOS!](https://github.com/henninghall/react-native-date-picker)

### 動畫效果
1. [maxs15/react-native-spinkit: A collection of animated loading indicators for React Native](https://github.com/maxs15/react-native-spinkit)：過場載入動畫圖
2. [joinspontaneous/react-native-loading-spinner-overlay: React Native loading spinner overlay](https://github.com/joinspontaneous/react-native-loading-spinner-overlay)

### 畫面開發工具

1. [necolas/react-native-web: React Native for Web](https://github.com/necolas/react-native-web)：可利用網頁方式顯示RN元件。
   1. [使用react-native-web將你的react-native應用H5化（一）](https://www.jianshu.com/p/917c35c0b0b4)完整說明使用方式與效果圖
   2. [cloudZQY/rnweb: a react-native-web demo](https://github.com/cloudZQY/rnweb)實際展示效果專案