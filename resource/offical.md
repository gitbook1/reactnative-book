# React-Native或Expo官方技術文件

## React-Native
- [RN官方有關CameraRoll操作](https://facebook.github.io/react-native/docs/cameraroll.html)


## Expo
* [Expo FileSystem](https://docs.expo.io/versions/latest/sdk/filesystem/)
> 可以取得檔案資訊
* [image-upload-example/App.js](https://github.com/expo/image-upload-example/blob/master/frontend/App.js)
> PRTElearning實際使用程式範本，包含如何格式化要上傳的檔案轉成stream形式form post處理