# 功能性導向元件

##硬體資訊偵測
- [react-native-device-info](https://github.com/rebeccahughes/react-native-device-info)
  - 若僅僅偵測平台、版本與裝置類型可以直接使用`Platform`元件

##快取技術
- [react-native-cache](https://github.com/timfpark/react-native-cache#readme)
  - 封裝React Native's AsyncStorage之上 (or included MemoryStore)

##檔案系統
* [itinance/react-native-fs: Native filesystem access for react-native](https://github.com/itinance/react-native-fs#existsassetsfilepath-string-promiseboolean)：判斷檔案是否存在


##裝置狀態判斷
* [rgommezz/react-native-offline](https://github.com/rgommezz/react-native-offline#readme)：deal nicely with offline/online connectivity
* [ChildishDanbino/react-native-detect-device](https://github.com/ChildishDanbino/react-native-detect-device/blob/master/index.js)：判斷裝置常用資訊，包含類型、平台、尺寸、螢幕比率(由於程式極簡未來應可方便擴充！)

##表單登入驗證
  - [登入驗證流程](https://github.com/spencercarli/react-navigation-auth-flow)
  - [影片說明](https://www.youtube.com/watch?v=L0ZsVjh2zBo)
  - [React Native Authentication in Depth Part 2 — Real World Auth Flow](https://medium.com/react-native-training/react-native-authentication-in-depth-part-2-real-world-auth-flow-a8a7c3aea096)
  - [React Native封装Form表单组件](https://www.jianshu.com/p/528be8872f34)
  - 元件間傳遞變數：父元件參數透過子元件變更
    - [Basic example to pass values between parent and child components in React.](https://gist.github.com/sebkouba/a5ac75153ef8d8827b98)
    - [Handling Multiple Form Inputs in React](https://medium.com/@tmkelly28/handling-multiple-form-inputs-in-react-c5eb83755d15)
  - [benhurott/react-native-masked-text](https://github.com/benhurott/react-native-masked-text#readme):直接限制輸入欄位格式，例如信用卡、電話輸入欄位等等。

##繪圖存檔(觸控簽名)
  - [RepairShopr/react-native-signature-capture](https://github.com/RepairShopr/react-native-signature-capture): A simple modular component for react native (iOS) to capture a signature as an image

##多語系處理
  - [Internationalization in React Native | What did I learn](https://whatdidilearn.info/2019/01/20/internationalization-in-react-native.html)

##測試開發工具
[ngrok - secure introspectable tunnels to localhost](https://ngrok.com/product)：本地開發環境也可以使用/測試第三方服務的利器
