# React-Native範例專案

---

* [Doctor Who? (Mobile App)](https://github.com/kuralabs/reactive-core-doctor-who-mobile )
  1. 如何架構專案目錄與檔案
  2. 如何取得資料
  3. 如何切換畫面等
* [Reflux框架架構](https://github.com/filp/react-native-es6-reflux)
  * 以ES6語法架構
* [react-native-login-screen](https://github.com/dwicao/react-native-login-screen)
  * 以Reflux架構實作的登入流程範例程式
* [React-Native-Demo](https://github.com/gingerJY/React-Native-Demo)
  * 2018年專案可以測試看看


###已實作過的基礎入門練習專題

* [react-native-app-with-authentication-and-user-management-in-15-minutes](https://scotch.io/tutorials/react-native-app-with-authentication-and-user-management-in-15-minutes)