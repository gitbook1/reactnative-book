# 深入了解Promise技術/語法

##重點觀念

1. Promise用來確保呼叫非同步處理時能夠依照預期的順序進行產生的技術。
2. 在javascript早期（2015年前）尚未發展出新一代語法ES6時大部分以第三方函式庫形式實現類似概念。
   1. Q
   2. when
   3. WinJS
   4. RSVP.js
3. `thenable`術語可說明應用Promise時的概念，上一個處理完成後包含返回結果傳遞串接下一個
4. 參考以下片段，使用Promise.resolve（與舊版/第三方未導入Promise函式庫整合）需注意，每次then()僅接受一個參數忽略其後其他參數
```javascript
var jsPromise = Promise.resolve($.ajax('/whatever.json'))
```
5. then() 包含兩個參數：一個用於成功，一個用於失敗（按照 promise 中的說法，即執行和拒絕），使用catch()相當於簡化取代`then(undefined, func)`寫法，也就是拒絕情況發生時


以下來自說明文件的範例程式可理解使用Promise的威力

```javascript
asyncThing1().then(function() {
  return asyncThing2();
}).then(function() {
  return asyncThing3();
}).catch(function(err) {
  return asyncRecovery1();
}).then(function() {
  return asyncThing4();
}, function(err) {
  return asyncRecovery2();
}).catch(function(err) {
  console.log("Don't worry about it");
}).then(function() {
  console.log("All done!");
})
```

對應流程圖
![流程圖](https://developers.google.com/web/fundamentals/primers/imgs/promise-flow.svg?hl=zh-tw)

> **特別注意**在`.catch()`後仍有一個`.then()`相當於jQuery的.done()概念。

在網址裡面最下方有使用Promise方法的總表可參考，最主要在實務上要懂得為何使用 `new Promise` 以及 呼叫 `Promise.all()` 原因。
從[這個地方開始看有助於理解上述應用實例](https://developers.google.com/web/fundamentals/primers/promises?hl=zh-tw#_9)

### 參考
* [Google Developers Promise說明文件](https://developers.google.com/web/fundamentals/primers/promises?hl=zh-tw)