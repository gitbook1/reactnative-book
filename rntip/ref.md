#React(React Native) Ref使用方法

```javascript
<AppContainer
  ref={nav => {
    this.navigator = nav;
  }}
/>
```

####參考
1. [ref](https://facebook.github.io/react/docs/refs-and-the-dom.html#the-ref-callback-attribute)