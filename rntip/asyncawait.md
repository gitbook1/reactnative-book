# Async與Await使用概念

##重點節錄
* **Async/Await 被規範在 ECMAScript 2016(ES7 or ES.Next) 的標準**
* **(2012年)ES2015/ES6以前使用Promise作法**實現非同步技術
* 使用**Promise.all可以並列呼叫**執行非同步請求/處理，**每一次await則是串接式請求**/處理


###待理解觀念
1. 呼叫了await就一定會有async? **不是**，參考以下來自 [expo/new-project-template](https://github.com/expo/new-project-template/blob/3384700874785f34a955ac9d90f870dbc2914d95/App.js)

```javascript
  ...

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  };

  ...
```
  我個人解讀，await與new Promise相當，都是宣告必須等待結果的語法，async則是宣告非同步函式，因此以上述案例，實際上是使用async與 Promise.all()搭配實作非同步請求。

2. then()...catch() 鍊式語法的問題：非同步函數簽章需完整契合，這裡講的**函數簽章**是殺小？


###附加資訊
補完 Promise 的相關函數
1. .finally()
   1. 除了 .then() 和 .catch()外，還有一個 .finally()， 這是不論怎麼是 reject / resolve 都一定會回呼，
   2. 通常用在進行一些事後處理或清理用不到的資料。
2. [bluebird](https://github.com/petkaantonov/bluebird)
   1. 標準 Promise 提供建立 Promise 物件的建構函數只有：Promise.resolve()、Promise.reject()、Promise.all()、Promise.race()。有點不夠用
      1. Promise.resolve()：
      2. Promise.all()：所有Promise物件完成後接續處理。
      3. Promise.race()：特定Promise物件完成後就優先處理。
   2. bluebird 提供更多好用的 Promise 相關函數，像是要求 map 產生的 Promise 元系依序執行 Promise.mapSeries()…等

###參考
1. [異步函數 - 提高 Promise 的易用性  | Google Developers](https://developers.google.com/web/fundamentals/primers/async-functions?hl=zh-tw)：以抓取log紀錄為案例詳盡解釋使用async/await思考流程
2. [別這樣使用Promise – Frochu – Medium](https://medium.com/frochu/%E5%88%A5%E9%80%99%E6%A8%A3%E4%BD%BF%E7%94%A8promise-d4f5a731adb4)
3. [從Promise 昇華到 async/await - iT 邦幫忙::一起幫忙解決難題，拯救 IT 人的一天](https://ithelp.ithome.com.tw/articles/10201420?sc=iThelpR)：針對Promise.reject / try{}catch{}以及throw error部分的說明值得參考。
4. https://www.valentinog.com/blog/how-async-await-in-react/
