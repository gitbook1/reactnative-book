#概念：物件變數根據子鍵值進行排序處理

時機：組成格式化物件過程可能因為特定邏輯判斷導致產出的物件並未保留原本順序狀況

作法：透過物件本身並不具備排序的邏輯操作，故要轉換成陣列再行處理。(_跟python類似_)

[Sorting JavaScript Object by property value](https://stackoverflow.com/questions/1069666/sorting-javascript-object-by-property-value)

