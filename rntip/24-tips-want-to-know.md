#[24 tips for React Native you probably want to know](http://albertgao.xyz/2018/05/30/24-tips-for-react-native-you-probably-want-to-know/)
這篇發布自2018-05-30的文章(大概在React-Native v0.55.x的情況)，以下節錄裡面對我而言相對重要的內容

##debug
1.1 **Breakpoint**
When IDE goes mad, like the Visual studio code or Webstorm won’t stop at the breakpoint, besides figuring it out by walking through their Github issue. You can simply put a debugger in your code, then it will stop the execution during runtime.

1.2 Attach the packager process
If you use `react-native-tools` for Visual Studio code. You just go to the debug tab and run that `Attach to packager` command and reload the app.
  在vscode安裝**react-native-tools**外掛，然後依照下列影片流程操作測試

  **[Debugging create-react-native-app with VSCode](https://www.youtube.com/watch?v=0_MnXPD55-E)**
  {%youtube%}0_MnXPD55-E{%endyoutube%}

- [ ] (需要測試)1.4 How to inspect the bridge
bridge between the native and js side. React native uses it to communicate between the two to do the UI updates and more. This is how you can inspect it. Just add the following code to your `index.js`:
```javascript
import MessageQueue from 'react-native/Libraries/BatchedBridge/MessageQueue.js';
const spyFunction = (msg) => {
  console.log(msg);
}
MessageQueue.spy(spyFunction)
```
Then in the console of your debugger, you should see lots of messages:
```javascript
Object {type: 0, module: "JSTimers", method: "callTimers", args: Array(1)}
```
The type field indicates the direction:
* If type equals 0 that means that the data is going from native to JavaScript;
* if it equals 1, the data goes from JavaScript to native.

1.5 Native debug, a must have
Xcode: After you running the app, Press the `Debug View Hierarchy` button, it will show all your views in a 3D way. You can inspect your full view tree in a very visual appealing way. This is a fantastic way to make yourself feel guilty because now you know how complex your UI actually is even though it seems to adopt a minimalist design concept.
![XcodeDebugViewHierarchy](../assets/XcodeDebugViewHierarchy.png)

以上節錄針對Debug in React-Native需要知道的基本作法與技巧