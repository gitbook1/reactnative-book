# React-native開發使用語法

##ES6語法

透過[ECMAScript 6: New Features: Overview and Comparison](http://es6-features.org/#Constants)可查大部分ES6使用到的新寫法

**重要！**
[JavaScript (ES6) Syntax 大集合](https://ithelp.ithome.com.tw/articles/10191712)

- Rest Operator 即 ...語法
Function 接受的參數數量不固定， Rest Operator 會把多的參數併成一個 Array。
```javascript
const foo = (x, y, ...args) => {
	console.log(x)
	console.log(y)
	console.log(args)
}
foo(1, 2, 3, 4, 5)

// output
// 1
// 2
// [3, 4, 5]


var foo = 'foo';
var obj = {
  foo: 'foo in Object'
};

var sayFoo = function() {
  console.log( this.foo );
};

obj.sayFoo = sayFoo;

obj.sayFoo();   // foo in Object
sayFoo();       // foo
```

- Spread Operator 用作解構 Array 或 Object
```javascript
const foo = (x, y, z) => {
	console.log(x)
	console.log(y)
	console.log(z)
}
// ...array2
// 可以看成 array[0], array[1], array[2] ...
let array = [1, 2, 3, 4]
foo(...array)
// 可以看成 foo(array[0], array[1], array[2] ...)

// oupput
// 1
// 2
// 3
```
補充：
  [javascript - Correct modification of state arrays in ReactJS - Stack Overflow](https://stackoverflow.com/questions/26253351/correct-modification-of-state-arrays-in-reactjs)

- 解構賦值(Destructuring Assignment)
```javascript
// ES6
let array = [1, 2, 3, 4]
let image = {
	width: 906,
	height: 512,
	src: "//cdn.static03.nicematin.com/media/npo/mobile_xlarge/2016/12/1-trump-1.jpg"
}
let [first, second, ...rest] = array
let { width, height, src, alt = "" } = image
```


- Enhanced Object Properties /
```javascript
let width = 906
let height = 512
let src =
	"//cdn.static03.nicematin.com/media/npo/mobile_xlarge/2016/12/1-trump-1.jpg"

// ES5
var image = {
	width: width,
	height: height,
	src: src
}

// ES6
let image = { width, height, src }
```
- 字串含變數串接 / Template Literals 在串接字串時特別好用，而且可以換行。

```javascript
// 簡略版本
`${params.name}'s Profile!`

let name = "wl00887404"
let birthday = "1996/4/19"

// ES5
console.log(
"Hello, " +	name + "!\nYour birthday is on " + birthday + ".\nHappy Birthday to you!"
)
// ES6
console.log(
`Hello, ${name}!
Your birthday is on ${birthday}.
Happy birthday to you!`
)
```