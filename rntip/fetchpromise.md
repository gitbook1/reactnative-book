# 改變fetch Promise判斷statusCode

原本使用fetch()語法通常是
```javascript
fetch(url)
.then(response => response.json())
.then(fetchJSON => {
  console.log("reponse :", fetchJSON); // <-------- res is ok with data
})
.catch(error => {
  console.wanr(error);
})
.done(() => {});
```
透過後端API回應statecode部分變化可以在發生錯誤時不導致程式中斷，而是顯示使用者訊息
```javascript
fetch(API.URL+peopleId,{ method: 'GET' })
.then((response) => {
  // 判斷statuscode丟出例外捕捉
  if( response.status == 403 ){
    throw "參數錯誤，無法取得物件資料！";
  }else if( response.status == 406 ){
    throw "您的帳號有406狀況發生，顯示錯誤訊息。";
  }
  return response.json();
})
.then(fetchJSON => {
  // 加入 iconsrc / 判斷 isSubmenu / 根據sort/position來排序
  dataJSONArray = Object.keys(fetchJSON).map(function (i, index) {
    let src = ''
    ...
    return {iconsrc: src, ...fetchJSON[index]};
  });
  // console.log( dataJSONArray );
  this.setState({
    dataSource: dataJSONArray,
  });
})
.catch((error) =>{
  this.setState({
    dataSource: (
      <View style={styles.center}>
        <Text h4>{error}</Text>
      </View>
    ),
  });
  // console.warn('擷取Activitymenu發生錯誤: '+error);
})
.done(() => {
  this.setState({
    isLoading: false,
  });
});
```