#讓fetch加入process進度作法

預設Fetch是不支援取得process的，必須使用XMLHttpReques才可以。

使用XMLHttpRequest方式，建立一個`api.js`
```javascript
const futch = (url, opts={}, onProgress) => {
    console.log(url, opts)
    return new Promise( (res, rej)=>{
        var xhr = new XMLHttpRequest();
        xhr.open(opts.method || 'get', url);
        for (var k in opts.headers||{})
            xhr.setRequestHeader(k, opts.headers[k]);
        xhr.onload = e => res(e.target);
        xhr.onerror = rej;
        if (xhr.upload && onProgress)
            xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
        xhr.send(opts.body);
    });
}
export default futch
```
Usage：
```javascript
import futch from './api';

const data = new FormData();
data.append('name', 'testName');
data.append('photo', {
  uri: source.uri,
  type: 'image/jpeg',
  name: 'testPhotoName'
});

futch(url, {
  method: 'post',
  body: data
}, (progressEvent) => {
  const progress = progressEvent.loaded / progressEvent.total;
  console.log(progress);
}).then((res) => console.log(res), (err) => console.log(err))
```

##使用Fetch（將上述程式整合到Fetch流程）

```javascript
import futch from './src/api';
const originalFetch = fetch
global.fetch = (url, opts) => {
  console.log(opts.onProgress)
  if (opts.onProgress && typeof opts.onProgress === 'function') {
    return futch(url, opts, opts.onProgress)
  } return originalFetch(url, opts)
}
export default class photoUploadTest extends Component {
 ...
}
```
Usage
```javascript
fetch(url + '/array', {
      method: 'post',
      body: data,
      onProgress: (e) => {
        const progress = e.loaded / e.total;
        console.log(progress);
        this.setState({
          progress: progress
        });
      }
    }).then((res) => console.log(res), (e) => console.log(e))
```