# 深入研究ReactNavigation

###待理解觀念
1. 使用porps.navigation.navigate()跟 porps.navigation.dispatch() 差異?
2. key跟routeName差別？
3. 是否能夠動態建立route討論[Adding new screens to navigation while app is running · Issue #310 · react-navigation/react-navigation](https://github.com/react-navigation/react-navigation/issues/310)

### 使用React-Native開發iOS程式

```bash
$yarn add react-navigation@lastest
# 必須要再加裝
```

### 頁面重新整理

在exprt實作方式是透過
```javascript
render() {
  let content = {};
  let self = this
  // 加入listen
  this._didFocusSubscription = this.props.navigation.addListener('willFocus', payload => {
    self._fetchPeople();
  });

  if ( this.state.isLoading === false ) {
    // render real content
  }else{
    // render pageLoading
  }
}
```

參考
* [How to refresh current screen when onPress · Issue #1924 · react-navigation/react-navigation](https://github.com/react-navigation/react-navigation/issues/1924)
* [Force re render on Back action · Issue #1617 · react-navigation/react-navigation](https://github.com/react-navigation/react-navigation/issues/1617)


### 語法

- 取得當下所有routes，必須是stack類型navigatior
```javascript
// 整個routes結構（不包含目前所在state）
this.props.navigation.dangerouslyGetParent().state.routes
```

- 重置routes作法
```javascript
// 其中配置兩個NavigationActions.navigate()是為了回上頁可以正確連結！
this.props.navigation.dispatch(StackActions.reset({
  index: 1,
  actions: [
  NavigationActions.navigate({routeName : 'routeName',}),
  NavigationActions.navigate({
    routeName : 'routeName',
    params : {
      aaa : 'bbb'
    }
  })],
}));
```

- 列出出目前頁面堆疊stack狀態/routes

必須是StactNavigation元件，可以透過
```javascript
this.props.navigation.dangerouslyGetParent().state
```
參考[how to get current route list?](https://github.com/react-navigation/react-navigation/issues/4349)

- 動態變更navigationOptions配置

例如判斷是否已登入來決定是否顯示登出按鈕？
參考[how to dynamically change navigationOptions · Issue #1274 · react-navigation/react-navigation](https://github.com/react-navigation/react-navigation/issues/1274)

```

```


##createDrawerNavigation-說明影片

{%youtube%}7uhJN4kVS6g{%endyoutube%}

[使用drawerNavigation建立展開形式選單](https://medium.freecodecamp.org/how-to-build-a-nested-drawer-menu-with-react-native-a1c2fdcab6c9)
概念是透過`DrawerNavigator`內的prop `contentComponent`建立側選單元件結構，同時控制每個listitems點選時判斷是否開啟次選單方式來處理。上述說明僅能做到2層選單，且主要是透過原本設定的drawerNavigation來建立listitems透過 **_** 分隔階層。


###參考
* [Different status bar configuration based on route](https://reactnavigation.org/docs/en/status-bar.html) / React-Navigation v3官方使用addListener事件範例
* React Navigation v3根據參數動態更新navigationOptions配置作法，例如判斷是否已登入來決定是否顯示登出按鈕？
[how to dynamically change navigationOptions · Issue #1274 · react-navigation/react-navigation](https://github.com/react-navigation/react-navigation/issues/1274)


使用React Navigation 頁面重新整理
[Force re render on Back action · Issue #1617 · react-navigation/react-navigation](https://github.com/react-navigation/react-navigation/issues/1617)

* [中文版可執行獨立專案](https://github.com/guangqiang-liu/react-navigation-demo)