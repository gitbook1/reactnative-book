#使用Typescript語法開發React Native


**global variable** `_`

##DefinitelyTyped
在 TypeScript 世界中，最熱門的 JavaScript 程式庫都使用 `.d.ts` 檔案描述其 API，而這類定義最常見的存放庫位於 DefinitelyTyped。
_For the most part, type declaration packages should always have the same name as the package name on npm, but prefixed with `@types/`, but if you need, you can check out <https://aka.ms/types> to find the package for your favorite library._

##設定tsconfig.json檔
[TypeScript Config](https://storybook.js.org/docs/configurations/typescript-config/)

##tslint.json檔