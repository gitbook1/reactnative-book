#概念：物件變數轉換成可逸代的陣列變數

時機：後端API取得JSON

問題點：從php phalcon端預設會產生全物件結構

```json
passJSON = { {var1:”data”}, {var2:”data2”} }
```
透過**react-native**的`fetch()`取得資料基本上沒問題，但在react的編譯器設計裡物件不支援逸代處理（Iterate）

使用
```javascript
for( var key in passJSON ){ }
```
或者說 只有 Array型態可以用
```javascript
.map(function(){})
```
故當取得物件時可以參考此篇做法進行其他邏輯處理[React convert props Objects into array then setState](https://stackoverflow.com/questions/45539619/react-convert-props-objects-into-array-then-setstate)


###補充
	[Looping Json & Display in React Native](https://stackoverflow.com/questions/34252982/looping-json-display-in-react-native)
    可以參考在render()內直接配置一個簡單的元件變數作法
