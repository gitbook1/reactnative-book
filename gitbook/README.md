###Gitbook Plugins整理

- [x] 1. Search Plus:支持中文搜尋
```json
{
    "plugins": ["search-plus"]
}
```
- [x] 2. Prism：在markdown語法加入強調標示效果樣式，可以再安装 [prism-themes](https://github.com/PrismJS/prism-themes) 外掛增加樣式效果
```json
{
    "plugins": [
        "prism",
        "prism-themes",
        "-highlight"
    ],
    "pluginsConfig": {
        "prism": {
            "css": [
                "prism-themes/themes/prism-base16-ateliersulphurpool.light.css"
            ]
        }
    }
}
```
- [x] 3. Emphasize：為文字加上底色
```json
{
   "plugins": [
      "emphasize"
   ]
}
```
- [x] 4. Puml：使用 PlantUML
```json
{
   "plugins": [
      "puml"
   ]
}
// 使用範例：
{% plantuml %}
Class Stage
    Class Timeout {
        +constructor:function(cfg)
        +timeout:function(ctx)
        +overdue:function(ctx)
        +stage: Stage
    }
    Stage <|-- Timeout
{% endplantuml %}
```
- [ ] 5. Anchors：增加 Github 風格錨點樣式
```json
{
   "plugins": [
      "anchors"
   ]
}
```
- [x] 6. Copy-code-button：為程式法區塊增加複製按鈕功能
```json
{
   "plugins": [
      "copy-code-button"
   ]
}
```
- [x] 7. youtubex：嵌入youtube影片外掛
```json
{
  "plugins": [
    "youtubex"
  ],
  "pluginsConfig": {
    "youtubex": {
      "embedDescription": {
        "en": "Watch this video!"
      }
    }
  }
}
```
  8. jsfiddle：插入JSFiddle外掛。
  9. jsbin：插入JSBin外掛。


####參考
* [gitbook-plugin](http://gitbook.zhangjikai.com/plugins.html)
* [Gitbook的使用和常用插件](https://zhaoda.net/2015/11/09/gitbook-plugins/)
* [GitBook 插件](https://github.com/zhangjikai/gitbook-use/blob/master/plugins.md)