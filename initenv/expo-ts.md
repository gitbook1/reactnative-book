#Expo專案使用ES7/TypeScript


- [ ] 整理以下此篇文章[Building a React Native App using Expo and Typescript (Part 1)](https://blog.expo.io/building-a-react-native-app-using-expo-and-typescript-part-1-a81b6970bb82)，根據標題流程配置屬於我自己的tsconfig.js / tslint.json等以及在package.json預先引入Typescript相關
- [ ] 透過上述文章第二篇，了解jtest(Unit Test)在React-Native開發指令與用途[Building a React Native App using Expo and Typescript (Part 2)](https://blog.expo.io/building-a-react-native-app-using-expo-and-typescript-part-2-778bf6599e3e)

###Expo+Typescrip範本
- [wainage/React-Native-TypeScript-Expo-CLI: Create a TypeScript enabled React Native app using Expo CLI v2](https://github.com/wainage/React-Native-TypeScript-Expo-CLI)
- [janaagaard75/expo-and-typescript: Demo app using Expo and TypeScript & Type definitions for the Expo SDK.](https://github.com/janaagaard75/expo-and-typescript)
- [b-tiwari/CRNAExpoTSExample: React-Native Example App with EXPO CRNA and Typescript](https://github.com/b-tiwari/CRNAExpoTSExample)