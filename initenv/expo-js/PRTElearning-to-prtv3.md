# 重構PRTElearning

由於最初開發PRTElearning時隨著時間持續更新使用ExpoSDK版本，最後完成時Expo的版本已經演進到v32.0.0。但從v31開始便無法支援Android 4.4.x+以上以及iOS9.0，且過程中為了sublime語法標示問題，改採用TypeScript同樣無法在上述平台下支援。因此決定重新再改寫一個以`v30.0.1`(對應RN v0.55.4)的App，命名為**prtv3**。


我根據此篇[Support init with a specific sdkVersion](https://github.com/expo/expo-cli/issues/142)最後提供的github專案範本，複製下來取出commit指定版本**3384700**另外建立一個[git專案](https://bitbucket.org/san_huang/expo-cli-template-30)作為以Expo SDK30.0.0環境使用的範本。_由於若要能夠支援expo-cli直接使用`expo init --template=xx`必須是發行到npm package上且符合expo專案架構(這又是另一個專題了)，所以使用上直接clone修改就好。_

###前置作業

首要解決使用snippet加速開發以及引入.jsx支援（這算是為了與一般web使用的js區別），主要要做的調整以下兩個步驟
1. /app.json內加入
```json
{
  ...
  "packagerOpts": {
    "projectRoots": "",
    "config": "rn-cli.config.js"
  }
}
```
若是Expo環境
```json
{
  "expo": {
    ...
    "packagerOpts": {
      "projectRoots": "",
      "config": "rn-cli.config.js"
    }
  }
}
```

2. 新增`/rn-cli.config.js`加入以下片段
```js
// 適用於rn版本 < 0.57
module.exports = {
  getSourceExts: () => ['jsx', 'js']
};

// 適用於rn版本 > 0.57 並增加建議配置
module.exports = {
  resolver: {
    sourceExts: ['js', 'json', 'ts', 'tsx', 'jsx']
  }
};
```

###補充

使用舊版SDK一開始執行`$yarn start`然後編譯在模擬器上App發生錯誤訊息**Can't Find Variable Self**，經查主要是相依的某個套件“whatwg-fetch”在當時有發布新版導致發生相容問題，必須要使用SDK 30.0.2才可以因此修改/package.json

```json
{
  "name": "sdk30",
  "jest": {
    "preset": "jest-expo"
  },
  "dependencies": {
    // 這裡從30.0.0改成30.0.1 or 30.0.2都可
    "expo": "^30.0.1",
    "react": "16.3.1",
    "react-native": "https://github.com/expo/react-native/archive/sdk-30.0.0.tar.gz",
    "react-navigation": "^2.16.0"
  },
  "devDependencies": {
    "jest-expo": "30.0.0"
  }
}

```