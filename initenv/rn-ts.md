# 原生React-Native與ES7/TypeScript

初步用來開發iOS版智慧家電控制介面App，主要以[Using TypeScript With React Native](https://medium.com/@jan.hesters/using-typescript-with-react-native-946aa4b4ae6f)使用以下指令

```bash
#  React Native CLI and [Emin’s template](https://github.com/emin93/react-native-template-typescript)
$react-native init ioshome --template typescript
# 建立範本後預設就已經設定好tsconfig.json，設定對Typescript語法檢查
$cd ioshome && yarn -D tslint tslint-eslint-rules tslint-react tslint-config-prettier
```

之後新增`/tslint.json`檔案
```typescript
{
  "extends": [
    "tslint:recommended",
    "tslint-eslint-rules",
    "tslint-react",
    "tslint-config-prettier"
  ],
  "jsRules": {},
  "rules": {
    "interface-name": false,
    "jsx-no-lambda": false,
    "object-literal-sort-keys": false,
    "quotemark": [true, "single", "jsx-double"]
  }
}
```

###Jest to Ts-jest
單元測試亦需要支援Typescript

補充
> change the lib key in your tsconfig.json from ["es6"] to ["es2017"] to have access to **newer syntax** such as Object.value
```json
// package.json
{
  "compilerOptions": {
    "allowJs": true,
    "allowSyntheticDefaultImports": true,
    "esModuleInterop": true,
    "isolatedModules": true,
    "jsx": "react",
    // 這邊修改成es2017
    "lib": ["es6"],
    "moduleResolution": "node",
    "noEmit": true,
    "strict": true,
    "target": "esnext"
  },
  "exclude": ["node_modules", "babel.config.js", "metro.config.js", "jest.config.js"]
}
```

```bash
# 執行程式啟動模擬器
$yarn start
# 執行tslint語法檢查
$yarn lint
```

##使用套件create-react-native-app

```bash
$ yarn global add create-react-native-app
$ create-react-native-app my-app --scripts-version=react-native-scripts-ts
$ cd my-app/
$ yarn start
```

###參考
[mathieudutour/create-react-native-app-typescript: Create a React Native app using Typescript on any OS with no build config.](https://github.com/mathieudutour/create-react-native-app-typescript)