#檔案上傳第三方套件

##rn-fetch-blob
A project committed to making file access and data transfer easier, efficient for React Native developers.
處理檔案上傳包含裝置本地端檔案系統資料存取或即時照片/影片存取，配置Content-Type使用application/octet，包含分割檔案上傳與進度確認。

[joltup/rn-fetch-blob](https://github.com/joltup/rn-fetch-blob)


##react-native-background-upload
Upload files in your React Native app even while it's backgrounded. Supports Android and iOS, including camera roll assets.
實現大型檔案(影片)背景持續處理後完成狀態通知（推播？）

[Vydia/react-native-background-upload](https://github.com/Vydia/react-native-background-upload)