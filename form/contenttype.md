#關於Content-Type詳細研究

表單POST使用content-type: multipart/formdata
[How to post multipart/formdata using fetch in react-native? - Stack Overflow](https://stackoverflow.com/questions/49625572/how-to-post-multipart-formdata-using-fetch-in-react-native?rq=1)

Content-Type形式
1. application/x-www-form-urlencoded


2. multipart/form-data：必須熟悉 boundary 用於分割字串長度與實際傳送資料不同，
```
POST http://www.example.com HTTP/1.1
Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryrGKCBY7qhFd3TrwA

------WebKitFormBoundaryrGKCBY7qhFd3TrwA
Content-Disposition: form-data; name="text"

title
------WebKitFormBoundaryrGKCBY7qhFd3TrwA
Content-Disposition: form-data; name="file"; filename="chrome.png"
Content-Type: image/png

PNG ... content of chrome.png ...
------WebKitFormBoundaryrGKCBY7qhFd3TrwA--
```

3. application/json：直接透過JSON()將變數結構轉json物件，以rawjson格式送出，語法： `Content-Type: application/json;charset=utf-8`
[問題]以application/json形式有辦法傳遞多媒體檔案或二進位檔案嗎？有辦法取得上傳進度嗎？

4. text/xml：用於XML-RPC請求形式，以XML檔案格式送出

####參考
