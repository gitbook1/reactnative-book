#apisauce

Axios + standardized errors + request/response transforms.
[infinitered/apisauce: Axios + standardized errors + request/response transforms.](https://github.com/infinitered/apisauce)

具備以下特點
* **low-fat** wrapper for the amazing axios http client library
* **all responses follow the same flow**: success and failure alike
* responses have a problem property to help guide exception flow
* attach functions that get called each request
* attach functions that change all request or response data
* detects connection issues (on React Native)

##範例

```javascript
import { create } from 'apisauce'

// create api.
const api = create({
  baseURL: 'http://localhost:3000',
})

// create formdata
const data = new FormData();
    data.append('name', 'testName');
    photos.forEach((photo, index) => {
      data.append('photos', {
        uri: photo.uri,
        type: 'image/jpeg',
        name: 'image'+index
      });
    });

// post your data.
api.post('/array', data, {
      onUploadProgress: (e) => {
        console.log(e)
        const progress = e.loaded / e.total;
        console.log(progress);
        this.setState({
          progress: progress
        });
      }
    })
      .then((res) => console.log(res))

// if you want to add DonwloadProgress, use onDownloadProgress
onDownloadProgress: (e) => {
  const progress = e.loaded / e.total;
}
```