# 開發模擬環境

先下載安裝Android Studio完成安裝

配置環境參數
```bash
$vim ~/.bash_profile

###...in vim file###
# OSX Mojave必須到Oracle官網下載JAVA安裝jre8才會有以下路徑
export JAVA_HOME=/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home

# 設定(給expo)快速從終端機載入Android裝置模擬器全域變數
export ANDROID_SDK=/Users/san/Library/Android/sdk
export ANDROID_TOOLS=/Users/san/Library/Android/sdk/platform-tools
export PATH="$ANDROID_TOOLS:$ANDROID_SDK/tools/bin:$PTOOLS_PATH:$PATH"
###...in vim file###

```

使用vscode安裝外掛`Android iOS Emulator`可以直接透過`Cmd+Shift+P`組合鍵`>Emulator`直接開啟Adnroid指定模擬器


---

###補充

如何使用舊版模擬器與舊版Expo client？

- 先取得舊版Expo Client，參考[Downgrade to older Expo client?](https://forums.expo.io/t/downgrade-to-older-expo-client/9328)內提供的[expo.io連結](https://expo.io/--/api/v2/versions)可以下載到指定sdk版本對應的`androidExpoView`，載回來是.tar.gz壓縮欓，解壓後會是一個**Android專案程式目錄結構**
- 取得.apk欓後如何安裝在模擬器/實機？
  - [How do you install an APK file in the Android emulator? - Stack Overflow](https://stackoverflow.com/questions/3480201/how-do-you-install-an-apk-file-in-the-android-emulator)