#Android開發環境

###OSX設定Android SDK路徑

在OSX安裝完Android Studio之後，主要要在安裝SDK / Avd manager 等以便可以啟動Android模擬器，同時由於Expo/RN主要直接透過cli呼叫執行android相關指令，亦必須正確配置環境變數以便找到指令位置

```bash

$vim ~/.bash_profile

# 在profile內容裡新增此行（OSX 10.14）
export JAVA_HOME=/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home

# 我另外指定由Android Stroid安裝後附帶安裝的SDK與相關platform-tools指令路徑
export ANDROID_SDK=/Users/san/Library/Android/sdk
export ANDROID_TOOLS=/Users/san/Library/Android/sdk/platform-tools

# 然後再附加上PATH以便cli呼叫
export PATH="$ANDROID_TOOLS:$ANDROID_SDK/tools/bin:$PATH"
```

**參考**
[java - What should I set JAVA_HOME to on OSX](https://stackoverflow.com/questions/1348842/what-should-i-set-java-home-to-on-osx)


###終端機直接指令開啟Android模擬器

1. 先切換到 `/Android/sdk/tools` 目錄下
```bash
$cd ~/Library/Android/sdk/tools
```
2. 列出所有模擬裝置
```bash
$./emulator -list-avds
```
3. 執行指定模擬器
```bash
$./emulator -avd avd_name [ {-option [value]}
```

###補充
> 以上可以轉成alias設定在bash_profile內