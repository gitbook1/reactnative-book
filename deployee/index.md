#部署發行與CI方面技巧

##使用yarn --production減少node_modules目錄大小

```bash
# 原本使用
$yarn install
# 加入 --production
$yarn install  --production
```

參考
1. [減少 node_modules 大小來加速部署 Node.js 專案](https://blog.wu-boy.com/2017/06/downsize-node_modules-to-improve-deploy-speed/)
2. [多个 webpack 项目, 如何共用依赖? - M.M.F 小屋](https://www.mmxiaowu.com/article/5848256cd4352863efb55476)

##輸出App

**2019-05-23**
>找到這個工具套件[fastlane - App automation done right](https://fastlane.tools/)可能是純RN開發情況下最佳作法？

以下文章都大略研究過與測試過，透過Expo實際輸出iOS仍必須付年費，而單純以Xcode輸出方式尚未實際測試過

**產生manifest檔**
```
itms-services://?action=download-manifest&url=https://raw.githubusercontent.com/PERSONALWORK/prtelearn_ios_ipa/master/prtv2.plist
```


參考
* [IPA 的 OTA 之旅，讓你的 app 不透過其他方式直接安裝到手機 – Nick’s journey on iOS. – Medium](https://medium.com/@zhongwei0717/ipa-%E7%9A%84-ota-%E4%B9%8B%E6%97%85-%E8%AE%93%E4%BD%A0%E7%9A%84-app-%E4%B8%8D%E9%80%8F%E9%81%8E%E5%85%B6%E4%BB%96%E6%96%B9%E5%BC%8F%E7%9B%B4%E6%8E%A5%E5%AE%89%E8%A3%9D%E5%88%B0%E6%89%8B%E6%A9%9F-749b02f061d7)
* [How to create .ipa file using Xcode? - Stack Overflow](https://stackoverflow.com/questions/5499125/how-to-create-ipa-file-using-xcode)：這次愛富案件後期應可測試。
* [iOS 如何利用 AdHoc 發佈 App – Esther – Medium](https://medium.com/@esther.tsai/ios-%E5%A6%82%E4%BD%95%E5%88%A9%E7%94%A8-adhoc-%E7%99%BC%E4%BD%88-app-648f1344b8a6)：登入線上Apple Developer管理介面進行profile檔案
* [【iOS - App上架流程圖文教學】 – 法蘭克的iOS世界 – Medium](https://medium.com/@mikru168/ios-app%E4%B8%8A%E6%9E%B6%E6%B5%81%E7%A8%8B%E5%9C%96%E6%96%87%E6%95%99%E5%AD%B8-724636ddc78b)
* [iOS App 上架流程, (2/3) 產出 .P12 憑證與 Provisioning Profile 檔案 - 大兵萊恩 一路直前](https://gogoprivateryan.blogspot.com/2015/08/ios-app-23-p12-provisioning-profile.html)
* [iOS推播通知憑證申請流程 – Collyn Chen – Medium](https://medium.com/@chiaping.collyn/ios%E6%8E%A8%E6%92%AD%E9%80%9A%E7%9F%A5%E6%86%91%E8%AD%89%E7%94%B3%E8%AB%8B%E4%BD%9C%E6%A5%AD-b5a3e51486a1)
* [發布測試版 iOS App 的 3 種方式 – Daniel Tsai – Medium](https://medium.com/@ck.camper/%E7%99%BC%E5%B8%83%E6%B8%AC%E8%A9%A6%E7%89%88-ios-app-%E7%9A%84-3-%E7%A8%AE%E6%96%B9%E5%BC%8F-413b8ba546fe)
  * [ipa在线下载安装（itms-services） - 杨小扬的专栏 - CSDN博客](https://blog.csdn.net/xlyrh/article/details/79078271)：實測似乎不可行，主要可能是manifest檔案結構問題，可能蘋果會檢查是否有正確的參數。
  * [iTunes 12.7 移除了 Apps 的選項，我該如何安裝 .ipa 檔案到 iOS 裝置？ – Lefty Chang – Medium](https://medium.com/@lefty./itunes-12-7-%E7%A7%BB%E9%99%A4%E4%BA%86-apps-%E7%9A%84%E9%81%B8%E9%A0%85-%E6%88%91%E8%A9%B2%E5%A6%82%E4%BD%95%E5%AE%89%E8%A3%9D-ipa-%E6%AA%94%E6%A1%88%E5%88%B0-ios-%E8%A3%9D%E7%BD%AE-2cad1d35d017)


##Expo環境輸出iOS ipa檔並自行提供必要檔案

**參考**
1. [Not able to create IPA file using command 'exp build:ios -c' - Help: Expo SDK - Forums](https://forums.expo.io/t/not-able-to-create-ipa-file-using-command-exp-build-ios-c/6885)
2. [(22) HOW to EXPORT IPA without paid developer account! [2018] - YouTube](https://www.youtube.com/watch?reload=9&v=5gRp5Z0tpXM)
3. [exp build:ios cant generate ipa without paid account · Issue #1528 · expo/expo](https://github.com/expo/expo/issues/1528)