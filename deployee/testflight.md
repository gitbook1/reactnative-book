# 提交iOS測試程式

## 透過Expo產出iOS版本應用程式安裝檔ipa

先編輯好`app.json`以及`package.json`內新版本的資訊與描述，使用以下指令產出檔案

```bash
$ expo build:ios
```

## 取得線上編譯的結果

透過expo編輯會產生兩個網址（若成功的話），第一個是編譯過程紀錄，經過一段時間後，則會產出第二個完成編譯可下載的網址，直接複製該網址貼到瀏覽器即可下載ipa檔。

## 開啟Xcode的開發者工具Application Loader

![step1-開啟Xocde](../assets/xcode-step1.png)

![step2-點選工具列Xcode下的Open Developer Tool](../assets/xcode-step2.png)

![step3-選擇Application Loader](../assets/xcode-step3.png)

## 直接點選Application Loader中間的圖示開始提交

![step4-Application Loader預設畫面](../assets/xcode-app-loader-step0.png)

![step50-選擇檔案](../assets/xcode-app-loader-step1.png)

![step51-過程紀錄](../assets/xcode-app-loader-step2.png)

![step52-過程紀錄](../assets/xcode-app-loader-step3.png)

![step53-過程紀錄](../assets/xcode-app-loader-step4.png)

![step54-過程紀錄](../assets/xcode-app-loader-step5.png)

![step55-過程紀錄](../assets/xcode-app-loader-step6.png)

![step56-完成上傳](../assets/xcode-app-loader-step7.png)

![step57-結束](../assets/xcode-app-loader-step8.png)

## 登入線上App Store Connect確認已完成發布測試版本

![step6-AppStoreConnect特定App的活動選單建置版本紀錄](../assets/appstore-connect-activity.png)

### 補充

在iOS裝置(發布測試App所屬的developer自己的帳號下)會收到新版本可以更新的推播訊息
![ios裝置的推播](../assets/ios-testflight-notification.jpeg)