#Expo使用Rect-Native-Debugger

首先，在vscode sidebar找到`/.expo/packager-info.json`內有
```json
{
  "devToolsPort": 19002,
  "expoServerPort": 19000,
  "packagerPort": 19001,
  "packagerPid": 7057,
  "expoServerNgrokUrl": "https://4s-fbz.personalwork.expoui.exp.direct",
  "packagerNgrokUrl": "https://packager.4s-fbz.personalwork.expoui.exp.direct",
  "ngrokPid": 7080
}
```
其中若要使用**Rect-Native-Debugger**除錯，必須於package.json內scripts片段配置如下：
```json
{
  "scripts": {
    "debug": "open 'rndebugger://set-debugger-loc?host=localhost&port=19001'"
  }
}
```

另外可使用[react-native-debugger-open - npm](https://www.npmjs.com/package/react-native-debugger-open)，先安裝react-native-debugger-open套件，並於package.json配置scripts片段
```json
"scripts": {
  "postinstall": "rndebugger-open --expo"
}
```
附加的參數說明
Name | Description
-----|------------
macos | Use react-native-macos module name instead of react-native. Default is false
revert | Revert rndebugger-open injection. Default is false
open | Run open directly instead of inject patch
port | Specified react-native packager port with --open option. **Default is 8081**
expo | **Use Expo's RN packager port** if you're not specified port.