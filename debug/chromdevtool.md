#React-Native debug工具

2018年5月以前當時測試開發版本使用react-native v0.54.x，直接使用Chrome開啟網址 <http://localhost:8081/debugger-ui/> 並確認該頁籤唯一且獨立，勾選「Maintain Priority」

![Chrome DevTool](../assets/chromedevtool.png)

亦可安裝獨立GUI程式

```bash
$ brew cask install react-native-debugger
$ npm install -g react-devtools
# 執行方式
$ react-devtools
```