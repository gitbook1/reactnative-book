#Ract-Native除錯研究紀錄


##原生RN開發環境整合vscode以及react-native-debugger進行除錯
起步，以Create react native app（純react-native開發）形式建立專案，接著透過brew安裝react-native-debugger（擴充原本chrome的devTool產出的獨立GUI程式）
```bash
$brew update && brew cask install react-native-debugger
```
接著，切換到透過create-react-native-app產生的專案內重點是修改package.json內script片段加入debug指令
```json
// package.json
"scripts": {
  "start": "node node_modules/react-native/local-cli/cli.js start",
  "test": "jest"
},
// 改成
"scripts": {
  "start": "node node_modules/react-native/local-cli/cli.js start",
  "test": "jest",
  "debug": "open 'rndebugger://set-debugger-loc?host=localhost&port=19001'"
},
```

####參考
* [Debugging React Native Expo using react-native-debugger](https://streetsmartdev.com/debugging-react-native-expo-using-react-native-debugger/)
* [Bug Reporting for React Native-instabug](https://try.instabug.com/react-native/?utm_source=awesomereact&utm_medium=spon&utm_content=header)