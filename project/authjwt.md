# React-Native登入使用JWT機制傳送交換token

為何使用token以及所謂token交換機制的定義？
現行多半應用的加密演算法，針對php-phalcon / python / .net core後端運算機制可適用方式

JWT的由下面三個部分組成，每一個部分用點號.分開。

1. 標頭Header：加密類型alg及token類型typ，例如：
```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```
2. 酬載Payload：用來存放用來要求**資料的資訊(claims)**，例如使用者名稱等。claims有三種`reserved claims`，`public claims`及`private claims`
3. 簽名Signature：Signature的是以**編碼(Base64)過的header，payload及附加的密碼參數**來產生
所以JWT格式是`xxxxx.yyyyy.zzzzz`，分別代表上述1~3組成的編碼字串。

*

JWT機制
![JWT請求](https://goo.gl/JwkyRy)

JWT機制是stateless的，也就是使用者的狀態不會存在server端，也就是不使用session保存狀態，server端不會保存你是否曾經來過的資訊。且請求資源時的clinet必須主動提供token；而傳統session的驗證機制是stateful，也就是在server端會紀錄使用者的狀態，又session id是存放在cookie中，發送請求時cookie會自動隨著request發送，所以會有CSRF(Cross-site request forgery)的風險。
JWT另一個特色是本身即可存放被請求的資訊（也就payload的部分），這樣就可以減少對資料庫的連線請求。例如傳統使用token驗證時，server端通常會將token對照的使用者資訊存在資料庫中，所以必須拿token至資料庫比對查詢出使用者資訊後再回傳給client；而JWT的做法是payload本身即可裝載使用者資訊，client將JWT傳至server端驗證過後，即可將JWT解碼並取得payload並返回給使用者。

###使用RN(without EXPO)產生JWT token
**For iOS**, you'd store that in the keychain...
https://auth0.com/docs/libraries/lock-ios/save-and-refresh-jwt-tokens
Here's a couple ways of doing that in react native that I found. There may be others. There may be better options. This is just what I found quickly.[react-native+keychain](https://github.com/search?utf8=%E2%9C%93&q=react-native+keychain)

**For Android**, you'd store that in either the SharedPreferences or maybe even better the KeyStore since it's encrypted there.

##測試研究JWT技術
[JWT Auth with an Elixir on Phoenix 1.4 API and React Native Mobile App, Part I](https://medium.com/@njwest/jwt-auth-with-an-elixir-on-phoenix-1-3-guardian-api-and-react-native-mobile-app-1bd00559ea51)

[Building a React Native JWT Client: API Requests and AsyncStorage](https://medium.com/@njwest/building-a-react-native-jwt-client-api-requests-and-asyncstorage-d1a20ab60cf4)

###範例程式
  * [phalcon-jwt-auth](https://github.com/dmkit/phalcon-jwt-auth)：phalcon、micro、jwt
  * [expo-jwt: Encode and decode JSON Web Tokens (JWT) in an Expo based React Native project.](https://github.com/kartenmacherei/expo-jwt)

####參考
* [JWT (JSON Web Tokens)](https://matthung0807.blogspot.com/2017/11/jwt-json-web-tokens.html):詳細解釋為何使用JWT以及實作機制
* [以 JSON Web Token 替代傳統 Token](https://yami.io/jwt/)解釋為何使用JWT取代傳統token認證以及從傳統token取代session
* [react-native-pure-jwt](https://github.com/zaguiini/react-native-pure-jwt):A React Native library that uses native modules to work with JWTs!