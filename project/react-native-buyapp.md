#Best Buy App
E-Commerce mobile application for Android and iOS

##Features ✅
1. user can **sign up and login with Facebook, Google or e-mail**
2. view most popular and trending items in the store
3. view items by **category**
4. predictive **universal search**
5. **add to items to cart**