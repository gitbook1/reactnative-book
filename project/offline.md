#實作離線使用App

要考慮的情況：
- [ ] 如何偵測目前裝置網路連線狀態判斷是否需要使用離線資料
- [ ] 離線資料儲存結構與儲存方式
- [ ] 離線資料本身與快取技術是否相同？或是類似
- [ ] 何時判斷必須線上才能使用，顯示警示訊息

本地端`裝置本身`與`遠端Server/API`資料庫`PouchDB`/`CouchDB`應用


處理本地儲存機制
[sunnylqm/react-native-storage: local storage wrapper for both react-native and browser. Support size controlling, auto expiring, remote data auto syncing and getting batch data in one query.](https://github.com/sunnylqm/react-native-storage)

###參考
* [原生react-native偵測網路套件react-native-netinfo: React Native Network Info API for Android & iOS](https://github.com/react-native-community/react-native-netinfo)
* [Expo透過Netinfo API來處理](https://docs.expo.io/versions/latest/react-native/netinfo/)
* [偵測裝置連線狀態，確認可以離線儲存檔案](https://www.npmjs.com/package/react-native-network-info)
* [offline-first application using PouchDB and CouchDB](https://medium.com/yld-engineering-blog/building-a-react-native-offline-first-application-using-pouchdb-and-couchdb-fa4ae048431f)
* [数据存储之react-native-storage的简单使用](https://www.jianshu.com/p/0d2b95293e64)