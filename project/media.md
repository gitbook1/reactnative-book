# 製作一個可以觸發照相錄影並選擇儲存或是上傳到後端的程式
以此建立一個專案學習以下功能實作方法

需求
  * [按鈕觸發啟動錄影/拍照功能](https://github.com/expo/expo-docs/blob/8dfaa440335ab2a70ce609b8c72d99246791598b/versions/unversioned/sdk/camera.md)
  * 檔案上傳流程
  * [檔案儲存於裝置本地端](http://nobrok.com/file-upload-with-expo-and-firebase/)
    * 儲存路徑控制
    * 取得檔名與檔案資訊

#TODOs
- [ ] App連結(iOS與Android個別)原生函式庫可以開啟相機的程式片段。
  - [ ] 原生React Native使用`CameraRoll`元件來處理處理存取相機、相簿功能。
- [ ] 設定單純錄製或單純拍照、切換兩種模式的程式片段。
  - [ ] 拍照或錄影後直接存取檔案的程式片段。
  - [ ] 拍照或錄影後設定儲存路徑的程式片段，並了解不同平台處理檔案系統路徑作法。
- [ ] 輸出的檔案格式選擇作法


####參考
* [Record and Upload Videos with React Native](https://medium.com/react-native-training/uploading-videos-from-react-native-c79f520b9ae1)：參考基礎流程
* [React-Native-Tips/How_to_upload_photo,file_in react-native](https://github.com/g6ling/React-Native-Tips/tree/master/How_to_upload_photo%2Cfile_in%20react-native)
* [react-native-community/react-native-camera: A Camera component for React Native. Also supports barcode scanning!](https://github.com/react-native-community/react-native-camera)：可以直接處發拍照後處理
