# React-Native

[實作24個開發react-native前需要知道的技巧一文：除錯](rntip/24-tips-want-to-know.md)

## TODOs
[palmerhq/tsdx: Zero-config CLI for TypeScript package development](https://github.com/palmerhq/tsdx?utm_source=mybridge&utm_medium=blog&utm_campaign=read_more)

[序言 · iOS 12 App 程式設計進階攻略（試閱版）](https://www.appcoda.com.tw/intermediate-swift-tips/)
[iOS 12 App程式設計實戰心法 | 30個開發專業級iOS App的必學主題與實務講座](https://www.appcoda.com.tw/swift/)
書目內容介紹[Swift 及 iOS App 程式開發書籍 | 初學及進階都適合](https://www.appcoda.com.tw/books/)
[Swift.org - Welcome to Swift.org](https://swift.org/?source=post_page---------------------------)
[原生模块 · React Native 中文网](https://reactnative.cn/docs/native-modules-ios/)


如何處理 this.setState 與 async / await狀態！
[await setState in react-native – Alexander Kuttig – Medium](https://medium.com/@Alexander.Kuttig/await-setstate-in-react-native-631d182e8738)


[kevinwolfcr/formal: 👔 Elegant form management primitives for the react hooks era.](https://github.com/kevinwolfcr/formal?utm_source=mybridge&utm_medium=blog&utm_campaign=read_more#included-packages)
- [ ] [找出RN 0.57/0.58/0.59版本後對應支援最低的Android/iOS版本](version/rn.md)
- [ ] [[IOS] "fetch + FormData" create the Content-Type with a boundary wrapped with delimiter " · Issue #7564 · facebook/react-native](https://github.com/facebook/react-native/issues/7564):可能說明為何prt表單上傳影片、組合欄位有時會有問題狀況！
- [ ] 有關fetch API的深入研究資訊（推測對未來處理ajax技術有幫助！）
  - [x] What is the fetch method
    It is a WHATWG browser API specification. You can read more about at the following links:
    https://fetch.spec.whatwg.org/
    https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
  - [ ] The following options are currently not working with fetch
    - redirect:manual
    - credentials:omit
- [ ] [關於TypeScript definition的疑惑](rntip/typescript/dts.md)
- [ ] 在TypeScript語法下除了ts, tsx外，使用jsx, js時機？以及是否需要額外設定？一般來說如何解讀應該用tsx還是ts或者說退到用jsx情況？
- [ ] [How to Build a Real Time Logo Detection App with React Native, Google Vision API and Crowdbotics](https://medium.com/crowdbotics/how-to-build-a-real-time-logo-detection-app-with-react-native-google-vision-api-and-crowdbotics-9ed65fbcd15)

[Gitbook plugins整理](gitbook/README.md)

## Expo或原生RN模式

首先必須確認**要使用哪一種做法來開發App?**是原生RN環境還是透過Expo(SDK搭配expo-cli)，根據我的經驗，剛開始接觸或者專案需求確認不會用到很依賴底層硬體技術的（例如：需要用地圖定位、語音助理等等）直接用ExpoSDK。
以下列出四種開發方案作法說明：

####Expo模式
- [Expo專案使用ES6/Babel](initenv/expo-js.md)
  - [重構PRTElearning(prtv3)紀錄](initenv/expo-js/PRTElearning-to-prtv3.md)
- [Expo專案使用ES7/TypeScript](initenv/expo-ts.md)

####React-Native模式
- [原生RN專案使用ES6/Babel](initenv/rn-js.md)
- [原生RN專案使用ES7/TypeScript](initenv/rn-ts.md)

####使用react-native init

透過react-native init產生的預設專案目錄基本上是以babel7為基礎（2019-05-27），但並未加入typescript支援（語法檢查、編譯器），同樣針對ES7使用語法檢查同樣並未預設加入，因此建立專案並初始化版本後第一件事，便是配置使用`eslint`或是`tslint`加到devDependencies片段，以yarn為例：
**javascript版本**
參考[Development: Basic Project Linting](https://www.reactnativeschool.com/debugging-in-development-and-production/development-project-linting)
```bash
# 此處使用airbnb建議的檢查規則，詳細規則可以參考https://eslint.org/docs/2.0.0/rules/
$yarn add eslint eslint-config-handlebarlabs
```
**typescript版本**
直接參考官網說明安裝附加套件，[Using TypeScript with React Native · React Native](https://facebook.github.io/react-native/blog/2018/05/07/using-typescript-with-react-native)


###狀況紀錄



#### 相關參考資源
- [How to find package installed globally with YARN or npm](https://stackoverflow.com/questions/51696531/how-to-find-package-installed-globally-with-yarn-or-npm)
- [What is the difference between Expo and React Native?](https://stackoverflow.com/questions/39170622/what-is-the-difference-between-expo-and-react-native/39170715#39170715)
- [[SOLVED] rn-cli-config.js not working after update to SDK 31 - Help: Expo SDK - Forums](https://forums.expo.io/t/solved-rn-cli-config-js-not-working-after-update-to-sdk-31/15980)
- [`.jsx` extension cannot be used with React Native · Issue #982 · airbnb/javascript](https://github.com/airbnb/javascript/issues/982)
- [How to downgrade to a lower Expo SDK version - Help: Expo SDK - Forums](https://forums.expo.io/t/how-to-downgrade-to-a-lower-expo-sdk-version/2099)
- [javascript - React Native Expo Can't Find Variable Self - Stack Overflow](https://stackoverflow.com/questions/52269560/react-native-expo-cant-find-variable-self)
