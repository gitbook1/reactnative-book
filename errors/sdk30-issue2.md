# App版的應用程式錯誤

在逐步轉移原本PRTElearning（SDKv32.0.0）到現在prtv3（SDKv30.0.1）常常會出現以下錯誤：**The development server returned response error code: 500**

```javascript
The development server returned response error code: 500

URL: http://192.168.1.101:19001/node_modules/expo/AppEntry.bundle?platform=android&dev=true&minify=false&hot=false&assetPlugin=%2FUsers%2Fsan%2FDesktop%2FReactNative%2Fprtsdk30%2Fnode_modules%2Fexpo%2Ftools%2FhashAssetFiles.js

Body:
{"type":"InternalError","errors":[],"message":"Metro Bundler has encountered an internal error, please check your terminal error output for more details"}
processBundleResult
    BundleDownloader.java:268
access$200
    BundleDownloader.java:35
onChunkComplete
    BundleDownloader.java:195
emitChunk
    MultipartStreamReader.java:76
readAllParts
    MultipartStreamReader.java:154
processMultipartResponse
    BundleDownloader.java:179
access$100
    BundleDownloader.java:35
onResponse
    BundleDownloader.java:148
c
    RealCall.java:135
run
    NamedRunnable.java:32
runWorker
    ThreadPoolExecutor.java:1112
run
    ThreadPoolExecutor.java:587
run
    Thread.java:841

```

初步發現很大的可能都是在**載入components/modules檔路徑錯誤造成**
例如：原本PRTElearning使用 ./src/coms 改到 ./components下未對應調整的話就會發生上述錯誤。