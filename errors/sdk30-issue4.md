# 轉換prtv2到prtv3 Android版本發生的語法錯誤問題


```javascript
TypeError: TypeError: In this environment the sources for assign MUST be an object. This error is a performance optimization and not spec compliant.

This error is located at:
    in ActivitymenuScreen
    in withNavigation(ActivitymenuScreen) (created by SceneView)
    in SceneView (at StackViewLayout.js:795)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at StackViewCard.js:69)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at screens.native.js:59)
    in Screen (at StackViewCard.js:57)
    in Card (at createPointerEventsContainer.js:27)
    in Container (at StackViewLayout.js:860)
    in RCTView (at View.js:60)
    in View (at screens.native.js:83)
    in ScreenContainer (at StackViewLayout.js:311)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at StackViewLayout.js:307)
    in Handler (at StackViewLayout.js:300)
    in StackViewLayout (at withOrientation.js:30)
    in withOrientation (at StackView.js:79)
    in RCTView (at View.js:60)
    in View (at Transitioner.js:214)
    in Transitioner (at StackView.js:22)
    in StackView (created by Navigator)
    in Navigator (at createKeyboardAwareNavigator.js:12)
    in KeyboardAwareNavigator (created by SceneView)
    in SceneView (at DrawerView.js:149)
    in RCTView (at View.js:60)
    in View (at ResourceSavingScene.js:20)
    in RCTView (at View.js:60)
    in View (at ResourceSavingScene.js:16)
    in ResourceSavingScene (at DrawerView.js:148)
    in RCTView (at View.js:60)
    in View (at screens.native.js:83)
    in ScreenContainer (at DrawerView.js:138)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at DrawerLayout.js:380)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at DrawerLayout.js:379)
    in Handler (at DrawerLayout.js:430)
    in DrawerLayout (at DrawerView.js:165)
    in DrawerView (created by Navigator)
    in Navigator (created by SceneView)
    in SceneView (created by SwitchView)
    in SwitchView (created by Navigator)
    in Navigator (at createAppContainer.js:388)
    in NavigationContainer (at App.js:44)
    in App (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)

This error is located at:
    in NavigationContainer (at App.js:44)
    in App (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)

Stack trace:
  node_modules/react-native/Libraries/polyfills/Object.es6.js:39:10 in assign
  components/formelements.jsx:116:3 in render
  node_modules/react-proxy/modules/createPrototypeProxy.js:44:35 in proxiedMethod
  node_modules/react-native/Libraries/Renderer/ReactNativeRenderer-dev.js:8811:23 in finishClassComponent
  ...
```
實際上無法從上述訊息找出正確錯誤位置與原因，經逐步除錯發現主要是以下片段造成

```javascript
// 原Activitymenu.tsx內寫法
<ImageBackground style={{ flex: 1, ...styles.backgroundImage }}
```
此語法確定在typescript以及輸出iOS時並不會發生錯誤，但改用jsx(ES6)與android平台時會出現以下錯誤訊息

必須改成
