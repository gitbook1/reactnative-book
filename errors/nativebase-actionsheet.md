#使用ActionSheet技巧

發生錯誤：

![初次套用ActionSheet發生錯誤畫面](https://user-images.githubusercontent.com/20476002/33278123-d9bc2ae2-d39a-11e7-83d3-019f110df01d.png)

解法：
For ActionSheet to work, you need to **wrap your topmost component inside from native-base**. You need not wrap every screen in `<Root/>`.

因此原程式片段：
```typescript
import {
  Container,
  Content,
  Button,
  List,
  ListItem,
  Card,
  CardItem,
  ActionSheet,
  Accordion,
} from 'native-base';
...

  render() {
    return (
        <Container>
          <Content padder>
            <List>
              <ListItem>
                <Body>
                  <H2>[建案]住戶xxx, 您好</H2>
                  <Text note>(登入後狀態)可使用右側按鈕切換社區。</Text>
                </Body>
                <Right>
                    <Button small rounded dark
                    onPress={() => {
                      ActionSheet.show(
                          {
                            options: SheetItems,
                            cancelButtonIndex: CANCEL_INDEX,
                            destructiveButtonIndex: DESTRUCTIVE_INDEX,
                            title: "我的建案社區列表"
                          },
                          buttonIndex => {
                            this.setState({ clicked: SheetItems[buttonIndex] });
                          }
                        )
                      }
                    }
                  >
                    <Text>切換</Text>
                  </Button>
                </Right>
              </ListItem>
            </List>

          </Content>

          {<L.IFooter />}

        </Container>
      );
  }
```
改成
```typescript
import {
  Root,
  Container,
  Content,
  Button,
  List,
  ListItem,
  Card,
  CardItem,
  ActionSheet,
  Accordion,
} from 'native-base';
...

  render() {
    return (
      <Root>
        <Container>
          <Content padder>
            <List>
              <ListItem>
                <Body>
                  <H2>[建案]住戶xxx, 您好</H2>
                  <Text note>(登入後狀態)可使用右側按鈕切換社區。</Text>
                </Body>
                <Right>
                    <Button small rounded dark
                    onPress={() => {
                      ActionSheet.show(
                          {
                            options: SheetItems,
                            cancelButtonIndex: CANCEL_INDEX,
                            destructiveButtonIndex: DESTRUCTIVE_INDEX,
                            title: "我的建案社區列表"
                          },
                          buttonIndex => {
                            this.setState({ clicked: SheetItems[buttonIndex] });
                          }
                        )
                      }
                    }
                  >
                    <Text>切換</Text>
                  </Button>
                </Right>
              </ListItem>
            </List>

          </Content>

          {<L.IFooter />}

        </Container>
        </Root>
      );
  }
```

即可正常