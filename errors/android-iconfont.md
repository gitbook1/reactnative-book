#console.error: "fontFamily "Roboto_medium" is not a system font and has not been loaded through Font.loadAsync.

參考[fontFamily 'Roboto_medium' is not a system font and has not been loaded through Exponent.Font.loadAsync](https://github.com/GeekyAnts/NativeBase/issues/1466)，主要是在android平台某些svg使用字型繪圖產生的圖示必須要先載入字形檔才可使用。

```javascript
console.error: "fontFamily "Roboto_medium" is not a system font and has not been loaded through Font.loadAsync.

- If you intended to use a system font, make sure you typed the name correctly and that it is supported by your device operating system.

- If this is a custom font, be sure to load it with Font.loadAsync."
__expoConsoleLog
    RemoteConsole.js:80:37
error
    YellowBox.js:59:8
processFontFamily
    Font.js:24:16
diffProperties
    ReactNativeRenderer-dev.js:3543:38
diffProperties
    ReactNativeRenderer-dev.js:3554:8
createInstance
    ReactNativeRenderer-dev.js:3969:29
completeWork
    ReactNativeRenderer-dev.js:11772:12
completeUnitOfWork
    ReactNativeRenderer-dev.js:13902:10
performUnitOfWork
    ReactNativeRenderer-dev.js:14117:30
workLoop
    ReactNativeRenderer-dev.js:14129:41
renderRoot
    ReactNativeRenderer-dev.js:14226:15
performWorkOnRoot
    ReactNativeRenderer-dev.js:15193:17
performWork
    ReactNativeRenderer-dev.js:15090:24
performSyncWork
    ReactNativeRenderer-dev.js:15047:14
requestWork
    ReactNativeRenderer-dev.js:14925:19
scheduleWork
    ReactNativeRenderer-dev.js:14711:16
scheduleRootUpdate
    ReactNativeRenderer-dev.js:15429:15
render
    ReactNativeRenderer-dev.js:16142:20
renderApplication
    renderApplication.js:59:52
run
    AppRegistry.js:101:10
runApplication
    AppRegistry.js:195:26
__callFunction
    MessageQueue.js:349:47
<unknown>
    MessageQueue.js:106:26
__guard
    MessageQueue.js:297:10
callFunctionReturnFlushedQueue
    MessageQueue.js:105:17

```

解決方式，在預設載入元件例如(App.tsx)內配置以下片段

```typescript
import { Font, AppLoading } from "expo";

async componentWillMount() {
  await Font.loadAsync({
    Roboto: require("native-base/Fonts/Roboto.ttf"),
    Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf")
  });
  this.setState({ loading: false });
}

```