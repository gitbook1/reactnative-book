# Warning: Can’t call setState (or forceUpdate) on an unmounted component. This is a no-op, but it indicates a memory leak in your application. To fix, cancel all subscriptions and asynchronous tasks in the componentWillUnmount method.

出現狀況，使用react-navigation的addListener在_component will unmounted_時**不應該使用setState()**
```javascript
  // /PRTElearning/src/screen/chapters/Submenu.tsx
  render() {
    let content = {};
    let self = this
    // 加入listen
    ...
    this.props.navigation.addListener('willBlur', payload => {
      self.setState({isLoading:true, categoryId:null, dataSource:null});
    });

    ...
  }
```

參考[詳細說明](https://www.robinwieruch.de/react-warning-cant-call-setstate-on-an-unmounted-component/)

故只要移除上述程式片段即可。