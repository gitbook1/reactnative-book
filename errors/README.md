# 各類遭遇過錯誤狀況

##TODOs

----------

###觀念釐清與開發技巧
* [嘗試以.android以及.ios副檔名載入常數constants配置的樣式檔案失敗問題](./platformSpecExtIssue.md)
* [Expo使用NativeBase與expo-icon在android平台遇到錯誤](./android-iconfont.md)
* [重構轉換過程常出現The development server returned response error code: 500原因](./sdk30-issue2.md)


###特定套件技巧與狀況
* [NativeBase使用ActionSheet：必須以<Root>元件包覆](./nativebase-actionsheet.md)


###語法問題
* [Warning：在View之下的元件不能使用resizeMode樣式語法](./sdk30-issue3.md)
* [Warning：Can’t call setState (or forceUpdate) on an unmounted component.](./warning-setstate.md)
* [TypeError: In this environment the sources for assign MUST be an object.(似乎發生在Android平台)](./sdk30-issue4.md)
* [TypeError: undefined is not a function (evaluating 'Font.isLoaded(fontName)')錯誤](./sdk30-issue1.md)：使用@expo/vector-icons的FontAwsome造成
* [Module AppRegistry is not registered callable module (calling runApplication)](./appregistry-issue1.md)


###編譯錯誤
* [使用react-native執行android模擬器發生tool.jar找不到問題](./rnrun-android-tooljar-notfound.md)