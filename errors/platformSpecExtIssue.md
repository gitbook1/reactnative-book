# 以指定平台副檔名限制載入元件/變數作法議題

在prtsdk30專案內我嘗試將以下
```javascript
./constants/styles.jsx
```
改用
```javascript
./constants/styles.android.js
./constants/styles.ios.js
```
方式根據平台載入對應的樣式表。

但發生以下錯誤，主要是找不到模組問題
```javascript
Requiring unknown module "1299".If you are sure the module is there, try restarting Metro Bundler. You may also want to run `yarn`, or `npm install` (depending on your environment).

unknownModuleError
    require.js:244:15
loadModuleImplementation
    require.js:178:29
guardedLoadModule
    require.js:148:36
_require
    require.js:132:20
<unknown>
    ForgetPassword.jsx:18
loadModuleImplementation
    require.js:214:12
guardedLoadModule
    require.js:148:36
_require
    require.js:132:20
<unknown>
    AppNavigator.js:6
loadModuleImplementation
    require.js:214:12
guardedLoadModule
    require.js:148:36
_require
    require.js:132:20
<unknown>
    App.js:4
loadModuleImplementation
    require.js:214:12
guardedLoadModule
    require.js:148:36
_require
    require.js:132:20
<unknown>
    AppEntry.js:2
loadModuleImplementation
    require.js:214:12
guardedLoadModule
    require.js:141:45
_require
    require.js:132:20
global code
```

目前尚未了解主要原因以及如何處理
1. 根據平台載入樣式表檔案
2. 是否Platform-specific extensions作法只適用於元件模組？