#使用react-native執行android模擬器發生tool.jar找不到問題

狀況：
```bash
$ react-native run-android
info JS server already running.
info Building and installing the app on the device (cd android && ./gradlew app:installDebug)...
> Task :react-native-gesture-handler:compileDebugJavaWithJavac FAILED

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':react-native-gesture-handler:compileDebugJavaWithJavac'.
# 這裡是主要原因
> Could not find tools.jar. Please check that /Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home contains a valid JDK installation.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.

* Get more help at https://help.gradle.org

BUILD FAILED in 10s
15 actionable tasks: 15 executed
error Could not install the app on the device, read the error above for details.
Make sure you have an Android emulator running or a device connected and have
set up your Android development environment:
https://facebook.github.io/react-native/docs/getting-started.html
error Command failed: ./gradlew app:installDebug. Run CLI with --verbose flag for more details.
```

於是確認是否我本機真的沒有tools.jar
```bash
$locate tools.jar
/Applications/Android Studio.app/Contents/jre/jdk/Contents/Home/lib/tools.jar
/Applications/Android Studio.app/Contents/plugins/android/lib/sdk-tools.jar
/Users/san/.gradle/caches/jars-3/fa43a1ceb5e9e03a84a5092e6ebfd151/sdk-tools.jar
```
由此可知透過Android Studio是有安裝的，只是不在`/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home`此路徑下。原本打算直接使用`ln -s`方式連結tools.jar，但可能會有相容性問題?，所以先搜尋類似問題

根據此youtube作法
{%youtube%}W8gsavSbOcw{%endyoutube%}

由於之前確實曾經找到並下載安裝jdk1.6版(java6)，在我的環境執行 `/usr/libexec/java_home` 確實顯示出另一路徑 `/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home`

```bash
$vim ~/.bash_profile

# 在.bash_profile檔案內
...
#export JAVA_HOME=/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home
#此為jdk安裝後路徑，使用1.6版jdk
export JAVA_HOME=$(/usr/libexec/java_home)
...

$ echo $JAVA_HOME
/Library/Java/JavaVirtualMachines/1.6.0.jdk/Contents/Home
```
再次執行react-native發現提示必須使用java7以上版本才可。
```bash
$ react-native run-android
info JS server already running.
info Building and installing the app on the device (cd android && ./gradlew app:installDebug)...

FAILURE: Build failed with an exception.

* What went wrong:
Gradle 4.10.2 requires Java 7 or later to run. You are currently using Java 6.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output. Run with --scan to get full insights.

* Get more help at https://help.gradle.org
error Could not install the app on the device, read the error above for details.
Make sure you have an Android emulator running or a device connected and have
set up your Android development environment:
https://facebook.github.io/react-native/docs/getting-started.html
error Command failed: ./gradlew app:installDebug. Run CLI with --verbose flag for more details.
```
於是參考[此篇說明](https://docs.oracle.com/javase/8/docs/technotes/guides/install/mac_jdk.html)，必須到oracle官網下載新版的Java SE jdk.dmg安裝檔（此篇紀錄時間2019-05-21，我下載的版本是jdk-12.0.1_osx-x64_bin）連到<https://www.oracle.com/technetwork/java/javase/downloads/index.html>下載，參考附圖
![下載jdk](assets/jdk-download.png)

直接依照步驟安裝完成後，測試目前系統使用版本
```bash
# 原本的1.6.0版實際上還在，上述此篇說明內有提示切換jdk版本方法。
$ java -version
java version "12.0.1" 2019-04-16
Java(TM) SE Runtime Environment (build 12.0.1+12)
Java HotSpot(TM) 64-Bit Server VM (build 12.0.1+12, mixed mode, sharing)
```

最後，再次執行react-native指令編譯就可成功！
```
$ react-native run-android
info JS server already running.
info Building and installing the app on the device (cd android && ./gradlew app:installDebug)...
Starting a Gradle Daemon, 1 incompatible Daemon could not be reused, use --status for details

> Configure project :app
Observed package id 'system-images;android-22;google_apis;x86' in inconsistent location '/Users/san/Library/Android/sdk/.temp/PackageOperation05' (Expected '/Users/san/Library/Android/sdk/system-images/android-22/google_apis/x86')

> Task :react-native-gesture-handler:compileDebugJavaWithJavac
warning: [options] source value 7 is obsolete and will be removed in a future release
warning: [options] target value 7 is obsolete and will be removed in a future release
warning: [options] To suppress warnings about obsolete options, use -Xlint:-options.
Note: /Users/san/ReactNative/ifulife/Ifulife/node_modules/react-native-gesture-handler/android/src/main/java/com/swmansion/gesturehandler/react/RNGestureHandlerButtonViewManager.java uses or overrides a deprecated API.
Note: Recompile with -Xlint:deprecation for details.
Note: Some input files use unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.3 warnings
/Users/san/.gradle/caches/modules-2/files-2.1/com.squareup.okhttp3/okhttp/3.12.1/dc6d02e4e68514eff5631963e28ca7742ac69efe/okhttp-3.12.1.jar: D8: Type `org.conscrypt.Conscrypt` was n
ot found, it is required for default or static interface methods desugaring of `java.security.Provider okhttp3.internal.platform.ConscryptPlatform.getProvider()`
[adb]: * daemon not running; starting now at tcp:5037
[adb]: * daemon started successfully

> Task :app:installDebug
11:51:02 V/ddms: execute: running am get-config
11:51:03 V/ddms: execute 'am get-config' on 'emulator-5554' : EOF hit. Read: -1
11:51:03 V/ddms: execute: returning
Installing APK 'app-debug.apk' on 'Nexus6P(AVD) - 5.1.1' for app:debug
11:51:03 D/app-debug.apk: Uploading app-debug.apk onto device 'emulator-5554'
11:51:03 D/Device: Uploading file onto device 'emulator-5554'
11:51:03 D/ddms: Reading file permision of /Users/san/ReactNative/ifulife/Ifulife/android/app/build/outputs/apk/debug/app-debug.apk as: rw-r--r--
11:51:04 V/ddms: execute: running pm install -r -t "/data/local/tmp/app-debug.apk"
11:51:13 V/ddms: execute 'pm install -r -t "/data/local/tmp/app-debug.apk"' on 'emulator-5554' : EOF hit. Read: -1
11:51:13 V/ddms: execute: returning
11:51:13 V/ddms: execute: running rm "/data/local/tmp/app-debug.apk"
11:51:13 V/ddms: execute 'rm "/data/local/tmp/app-debug.apk"' on 'emulator-5554' : EOF hit. Read: -1
11:51:13 V/ddms: execute: returning
Installed on 1 device.

BUILD SUCCESSFUL in 43s
43 actionable tasks: 29 executed, 14 up-to-date
info Running /Users/san/Library/Android/sdk/platform-tools/adb -s emulator-5554 reverse tcp:8081 tcp:8081
info Starting the app on emulator-5554 (/Users/san/Library/Android/sdk/platform-tools/adb -s emulator-5554 shell am start -n com.ifulife/com.ifulife.MainActivity)...
Starting: Intent { cmp=com.ifulife/.MainActivity }
```