# view元件包覆image元件語法問題

在prt內顯示次選單元件是採用底圖搭配左圖示右文字方式然後調整位置處理，在重構過程發生以下錯誤

**Warning: Failed prop type: Invalid props.style key `resizeMode` supplied to `View`.**

```javascript
[21:58:25] Warning: Failed prop type: Invalid props.style key `resizeMode` supplied to `View`.
Bad object: {
  "resizeMode": "contain",
  "width": "95%",
  "height": "95%",
  "backgroundColor": "rgba(0, 0, 0, 0)",
  "alignItems": "center",
  "justifyContent": "center"
}
Valid keys: [
  "display",
  "width",
  "height",
  "start",
  "end",
  "top",
  "left",
  "right",
  "bottom",
  "minWidth",
  "maxWidth",
  "minHeight",
  "maxHeight",
  "margin",
  "marginVertical",
  "marginHorizontal",
  "marginTop",
  "marginBottom",
  "marginLeft",
  "marginRight",
  "marginStart",
  "marginEnd",
  "padding",
  "paddingVertical",
  "paddingHorizontal",
  "paddingTop",
  "paddingBottom",
  "paddingLeft",
  "paddingRight",
  "paddingStart",
  "paddingEnd",
  "borderWidth",
  "borderTopWidth",
  "borderStartWidth",
  "borderEndWidth",
  "borderRightWidth",
  "borderBottomWidth",
  "borderLeftWidth",
  "position",
  "flexDirection",
  "flexWrap",
  "justifyContent",
  "alignItems",
  "alignSelf",
  "alignContent",
  "overflow",
  "flex",
  "flexGrow",
  "flexShrink",
  "flexBasis",
  "aspectRatio",
  "zIndex",
  "direction",
  "shadowColor",
  "shadowOffset",
  "shadowOpacity",
  "shadowRadius",
  "transform",
  "transformMatrix",
  "decomposedMatrix",
  "scaleX",
  "scaleY",
  "rotation",
  "translateX",
  "translateY",
  "backfaceVisibility",
  "backgroundColor",
  "borderColor",
  "borderTopColor",
  "borderRightColor",
  "borderBottomColor",
  "borderLeftColor",
  "borderStartColor",
  "borderEndColor",
  "borderRadius",
  "borderTopLeftRadius",
  "borderTopRightRadius",
  "borderTopStartRadius",
  "borderTopEndRadius",
  "borderBottomLeftRadius",
  "borderBottomRightRadius",
  "borderBottomStartRadius",
  "borderBottomEndRadius",
  "borderStyle",
  "opacity",
  "elevation"
]
```

原本程式片段
```javascript
<View style={[styles.Wcenter, { marginLeft: 5, marginTop: 2 }]}>
  <Image
    style={styles.ItemMenuImageIcon}
    source={item.iconsrc}
  />
</View>
```
可改用
```javascript
<Image
  style={styles.ItemMenuImageIcon}
  source={item.iconsrc}
/>
// 並在styles.js內定義ItemMenuImageIcon

  ItemMenuImageIcon: {
    position:'relative',
    top:10,
    left:10,
    resizeMode: 'contain',
  },
```
使用postion方式調整位置