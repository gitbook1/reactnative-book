#使用vector-icons的FontAwsome發生錯誤

**錯誤訊息**：TypeError: undefined is not a function (evaluating 'Font.isLoaded(fontName)')

在套入原本layouts配置的`customTopMenu`與`customBottomMenu`時，會發生錯誤
```javascript
TypeError: TypeError: undefined is not a function (evaluating 'Font.isLoaded(fontName)')

This error is located at:
    in Icon (at layouts.jsx:71)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at TouchableOpacity.js:247)
    in TouchableOpacity (at layouts.jsx:66)
    in RCTView (at View.js:60)
    in View (at layouts.jsx:46)
    in RCTView (at View.js:60)
    in View (at layouts.jsx:45)
    in TopMenu
    in withNavigation(TopMenu) (at Submenu.jsx:119)
    in RCTView (at View.js:60)
    in View (at ImageBackground.js:64)
    in ImageBackground (at Submenu.jsx:117)
    in SubmenuScreen
    in withNavigation(SubmenuScreen) (created by SceneView)
    in SceneView (at StackViewLayout.js:795)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at StackViewCard.js:69)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at screens.native.js:59)
    in Screen (at StackViewCard.js:57)
    in Card (at createPointerEventsContainer.js:27)
    in Container (at StackViewLayout.js:860)
    in RCTView (at View.js:60)
    in View (at screens.native.js:83)
    in ScreenContainer (at StackViewLayout.js:311)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at StackViewLayout.js:307)
    in Handler (at StackViewLayout.js:300)
    in StackViewLayout (at withOrientation.js:30)
    in withOrientation (at StackView.js:79)
    in RCTView (at View.js:60)
    in View (at Transitioner.js:215)
    in Transitioner (at StackView.js:22)
    in StackView (created by Navigator)
    in Navigator (at createKeyboardAwareNavigator.js:12)
    in KeyboardAwareNavigator (created by SceneView)
    in SceneView (created by SwitchView)
    in SwitchView (created by Navigator)
    in Navigator (at createAppContainer.js:388)
    in NavigationContainer (at App.js:43)
    in App (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)

This error is located at:
    in NavigationContainer (at App.js:43)
    in App (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)
Icon
    createIconSet.js:16:34
constructClassInstance
    ReactNativeRenderer-dev.js:6569:28
updateClassComponent
    ReactNativeRenderer-dev.js:8733:31
beginWork
    ReactNativeRenderer-dev.js:9581:10
performUnitOfWork
    ReactNativeRenderer-dev.js:12924:25
workLoop
    ReactNativeRenderer-dev.js:12953:43
renderRoot
    ReactNativeRenderer-dev.js:12996:17
performWorkOnRoot
    ReactNativeRenderer-dev.js:13632:34
performWork
    ReactNativeRenderer-dev.js:13545:26
performSyncWork
    ReactNativeRenderer-dev.js:13506:16
requestWork
    ReactNativeRenderer-dev.js:13392:6
scheduleWorkImpl
    ReactNativeRenderer-dev.js:13259:24
scheduleWork
    ReactNativeRenderer-dev.js:13207:28
enqueueSetState
    ReactNativeRenderer-dev.js:6224:19
setState
    react.development.js:242:31
dispatch
    createAppContainer.js:352:22
<unknown>
    getChildNavigation.js:65:33
<unknown>
    AuthLoading.jsx:23:37
tryCallOne
    core.js:37:14
<unknown>
    core.js:123:25
<unknown>
    JSTimers.js:295:23
_callTimer
    JSTimers.js:152:6
_callImmediatesPass
    JSTimers.js:200:17
callImmediates
    JSTimers.js:464:11
__callImmediates
    MessageQueue.js:327:4
<unknown>
    MessageQueue.js:145:6
__guardSafe
    MessageQueue.js:314:6
flushedQueue
    MessageQueue.js:144:17
invokeCallbackAndReturnFlushedQueue
    MessageQueue.js:140:11
```
經查詢發現主要是在`@expo/vector-icons`問題上，由於此案是降版本使用Expo SDKv30.0.1環境，故在`package.json`內要把
```json
// package.json
"@expo/vector-icons": "^9.0.0",
// 改用
"@expo/vector-icons": "8.1.0",
```
並重新移除node_modules在安裝套件即可解決

###參考
1. [Font.isLoaded is undefined... · Issue #2512 · GeekyAnts/NativeBase · GitHub](https://github.com/GeekyAnts/NativeBase/issues/2512)
2. [[SOLVED] Font.isLoaded is not a function - @expo/vector-icons - Help: Expo SDK - Forums](https://forums.expo.io/t/solved-font-isloaded-is-not-a-function-expo-vector-icons/17787)