#原生react-native開發下，多次測試後發生以下狀況

##訊息
[Module AppRegistry is not registered callable module (calling runApplication)](https://stackoverflow.com/questions/43604603/module-appregistry-is-not-registered-callable-module-calling-runapplication)

##解決方案
**必須重啟node server**，找出目前port 8081的程序，[When running yarn start --reset-cache or react-native start --reset-cache` you might encounter the…](https://medium.com/@nagella_ranjith/when-running-yarn-start-reset-cache-or-react-native-start-reset-cache-you-might-encounter-the-fc6c06abeaf4)


####補充
* [How to clear react native cache?](https://medium.com/@abhisheknalwaya/how-to-clear-react-native-cache-c435c258834e)