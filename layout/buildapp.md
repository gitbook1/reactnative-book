# 製作應用程式畫面與圖示

##在Expo環境下
* [@expo/vector-icons directory](https://expo.github.io/vector-icons/):查詢可用的Icon名稱。

##純React-Native
* [React Native 使用 LandingPage 範例](https://github.com/chenbin92/React-native-example/issues/8)
* [react-native-vector-icons directory](https://oblador.github.io/react-native-vector-icons/):查詢可用的Icon名稱。


###參考
* [透過Xcode製作landing page畫面](https://medium.com/@kelleyannerose/react-native-ios-splash-screen-in-xcode-bd53b84430ec)