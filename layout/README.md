# 樣式佈局

- [ ] 將參考的確認對於flex語法的各種設定意義整理自己的心得紀錄於此處。
- [ ] 版面樣式 根據裝置個別產生styles變數套用，
  1. 透過延伸副檔名
  2. 透過元件內判斷資訊來載入指定style檔案
  3. 透過style內使用Platform來指定樣式語法

##偵測裝置資訊作法

方式一、RN內建模組／Platform，主要使用
```javascript
import {Platform, StyleSheet} from 'react-native';

// 直接判斷ios or android
const styles = StyleSheet.create({
  height: Platform.OS === 'ios' ? 200 : 100,
});

// 使用Platform.select
const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...Platform.select({
      ios: {
        backgroundColor: 'red',
      },
      android: {
        backgroundColor: 'blue',
      },
    }),
  },
});
```
詳細[介紹說明](https://facebook.github.io/react-native/docs/platform-specific-code.html)

方式二、使用原生模組判斷手機還是平板

```javascript
import { NativeModules } from 'react-native';
const { PlatformConstants } = NativeModules;
// phone, pad, tv, carplay and unknown
const deviceType = PlatformConstants.interfaceIdiom;
```

方式三、安裝第三方套件，[react-native-device-info](https://github.com/rebeccahughes/react-native-device-info)
```javascript
// 簡單擴充 Platform元件加入偵測實際裝置是否為手機還是平板做法
import DeviceInfo from 'react-native-device-info';

// iOS平台透過寬高比率判斷
function isTabletBasedOnRatio(){
  if( PixelRatio.get() > 1.6){
      return false;
  }else{
      return true;
  }
}

// Android平台直接呼叫 DeviceInfo.isTablet() 做bool判斷
```


參考
* [React Native: How to Determine if Device is iPhone or iPad - Stack Overflow](https://stackoverflow.com/questions/40249604/react-native-how-to-determine-if-device-is-iphone-or-ipad)



### 參考
1. [確認對於flex語法的各種設定意義](https://github.com/crazycodeboy/RNStudyNotes/blob/master/React%20Native布局/React%20Native布局详细指南/React%20Native布局详细指南.md)
2. [Position element at the bottom of the screen using Flexbox in React Native](https://medium.com/react-native-training/position-element-at-the-bottom-of-the-screen-using-flexbox-in-react-native-a00b3790ca42)
3. [Layout with Flexbox中國簡體字](https://www.infoq.cn/article/react-native-layout)