# Redux與React-Native開發

Redux is the most common choice of library for managing data in medium and large apps. Redux provides a `store object` which maintains the application state, and can **notify your React components when state changes**. Redux was designed with React in mind, and has official React bindings: `React Redux`. There are additional tools around Redux to provide: control over asynchronous events, data persistence (for offline usage, etc), and more powerful debugging.

[Redux官網](https://react-redux.js.org/)

[影片介紹redux概念與應用](https://egghead.io/lessons/react-redux-implementing-combinereducers-from-scratch)：分得相當詳細的引導流程，每隻影片約莫4~5分鐘講解一個觀念。


##待確認觀念
- [ ] 使用react-redux 是否仍要使用react-navigation套件來管理頁面切換？
  [atoami/react-native-navigation-redux-starter-kit: React Native Navigation(v2) Starter Kit with Redux, Saga, ESLint, Babel, Jest and Facebook SDK 😎](https://github.com/atoami/react-native-navigation-redux-starter-kit)
- [ ] 一個較詳盡介紹使用React搭配Redux流程
[Theme your Expo app with Redux and React Navigation](https://medium.com/@hmajid2301/theme-your-expo-app-with-redux-and-react-navigation-461020e5fc1e)
