#Next.JS

以Server-side方式開發，可搭配React作為UI開發，實作參考[Next.js之基础概念](https://segmentfault.com/a/1190000010311978)，原則上是針對web開發使用的一套模式，但似乎亦可應用於React-Native，參考[Next.js links with React Native for Web](https://gist.github.com/necolas/f9034091723f1b279be86c7429eb0c96)
主要具備以下特點：
  - **Server-rendered** by default
  - Automatic code splitting for faster page loads
  - **Simple client-side routing (page based)**
  - **Webpack-based** dev environment which supports Hot Module Replacement (HMR)
  - Able to implement with **Express or any other Node.js HTTP server**
  - Customizable with your own Babel and Webpack configurations

常用比對技術關鍵字：**MobX**

#Redux

#Stroybook


###參考
* [Next.js 8](https://nextjs.org/blog/next-8/#example-of-api-authentication)
* [vscode-recipes/Next-js at master · Microsoft/vscode-recipes](https://github.com/Microsoft/vscode-recipes/tree/master/Next-js)
* [Next.js & StoryBook (ㄧ)](https://ithelp.ithome.com.tw/articles/10193515)：介紹透過storybook建立客製化元件使用方案與效果展示頁面作法
* [Using Storybook with React Native](https://pusher.com/tutorials/storybook-react-native)