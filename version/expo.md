# Expo開發環境

---

### Expo Client App(測試程式)版本/發布日期與下載
版本 | 發布日期
-------|--------------
[2.10.4](https://www.apkmonk.com/download-app/host.exp.exponent/4_host.exp.exponent_2019-01-16.apk/) | Jan. 16, 2019
2.10.3 | Jan. 9, 2019
2.9.2 | Nov. 13, 2018
[2.8.0](https://www.apkmonk.com/download-app/host.exp.exponent/4_host.exp.exponent_2018-09-12.apk/) | Sept. 12, 2018／支援SDK v30.0.0
2.7.2 | Aug. 20, 2018
2.7.1 | July 26, 2018／支援SDK v29.0.0，July 4, 2018發布RN v0.56
2.6.4 | July 3, 2018
2.6.2 | June 12, 2018
2.5.2 | May 15, 2018
2.5.0 | April 25, 2018
2.4.0 | March 29, 2018
2.3.2 | March 7, 2018
2.3.0 | Jan. 18, 2018
2.2.0 | Dec. 12, 2017
2.1.2 | Nov. 30, 2017
2.1.0 | Nov. 16, 2017
2.0.0 | Oct. 18, 2017
[1.0.6](https://www.apkmonk.com/download-app/host.exp.exponent/5_host.exp.Exponent_2019-01-12.apk/) | Jan. 12, 2019／支援SDK v30.0.0(Android4.4)
1.0.2 | Jan. 10, 2019
1.0.1 | Jan. 7, 2019

###另一個下載位置
含下載版本支援最小Android版本說明[Expo Apk Download | APK Tools](https://apk.tools/details-exponent-1508114-apk/)