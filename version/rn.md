# React-Native版本比較

主要從v0.55開始（我開始實際接觸版本），v0.55發展到v0.55.4後直接跳v0.57版，期間發行了多項alpha版。
取得版本歷史時間軸與下載位址[Releases · expo/react-native · GitHub](https://github.com/expo/react-native/releases?after=v0.57.0-rc.1)

##TODOs
- [ ] v0.57最低支援iOS版本？
- [ ] v0.58與v0.59最低支援Android與iOS版本？


##簡易快速從RN舊版本升級到新版本的方式
透過[Upgrade RN apps](https://pvinis.github.io/purge-web/)所提供的工具直接選擇當下版本以及打算升級到指定的版本，會顯示出版本之間的package.json引入相依套件差異，直接手動修改package.json後，再透過指令`yarn upgrade`進行套件更新即可。

參考
* [The easiest way to upgrade React Native to the latest version](https://www.freecodecamp.org/news/easiest-way-to-upgrade-react-native-to-the-latest-version-daecd284cb18/)，


其中比較重要的對照

React-Native | Expo | Android | iOS
---------|---------|----------|---------
 [v0.54.0](https://github.com/facebook/react-native/releases/tag/v0.54.0)~2018-02發表 | [v26.0.0](https://blog.expo.io/expo-sdk-v26-0-0-is-now-available-2be6d9805b31) | N/A | N/A
 v0.55.0(2018-04-04發表) ~ [v0.55.4](https://github.com/facebook/react-native/releases/tag/v0.55.4)(2018-04-28發表) | [v27.0.0](https://blog.expo.io/expo-sdk-v27-0-0-is-now-available-898bf1e5b0e4) / [v28.0.0](https://blog.expo.io/expo-sdk-v28-0-0-is-now-available-f30e8253b530) / [v29.0.0](https://blog.expo.io/expo-sdk-v29-0-0-is-now-available-f001d77fadf) / [v30.0.0](https://blog.expo.io/expo-sdk-30-0-0-is-now-available-e64d8b1db2a7) | SDK 4.4+/API 19+ | iOS 9.0+
 [v0.57.x](https://github.com/react-native-community/releases/blob/master/CHANGELOG.md#057)(2018-09-22發表0.57.1) | [v31.0.0](https://blog.expo.io/expo-sdk-v31-0-0-is-now-available-cad6d0463f49) / [v32.0.0](https://blog.expo.io/expo-sdk-v32-0-0-is-now-available-6b78f92a6c52) | SDK 4.4+ | C3
 v0.58(2019-01-25發表) | [v32.0.0](https://blog.expo.io/expo-sdk-v32-0-0-is-now-available-6b78f92a6c52) | B3 | C3
 v0.59(2019-03-12發表) | [v32.0.0](https://blog.expo.io/expo-sdk-v32-0-0-is-now-available-6b78f92a6c52) | B3 | C4

##React-Native版本更新重點
連結網址：<https://github.com/react-native-community/releases/blob/master/CHANGELOG.md>，之後附加#Version可以快速查看該版本更動資訊

###v0.59.0~v0.59.8
* 更新了javascriptCore並支援64-bit來執行Android版本程式提升效能
* 引入hook技術，改變原先管理state方式與語法。

###v0.58.0~v0.58.6
_v0.58.4_ 與 _v0.58.5_ 除了修正Bug亦改善效能與增加部分元件屬性，詳情可到網址查看。
_v0.58.3_ 修正使用StatusBar元件問題
_v0.58.2_ 修正了某些v0.58發展程式碼片段整併問題
_v0.58.1_ 修正了 _v0.58.0_ 使用 `react-native run-ios` 上的問題

* Support for **mutual TLS** in WebKit
* Asset serving from directories besides **/assets**
* support for **publicPath to enable serving static assets** from different locations
* **TouchableBounce** now has a simple **bounce()** function that can be used to trigger a bounce without callbacks
* for iOS
  * **manually link JavaScriptCore.framework** when upgrading following the steps shown [here](https://camo.githubusercontent.com/c09cd42747364b498efa7c82fcb73978ba076eae/687474703a2f2f646f63732e6f6e656d6f62696c6573646b2e616f6c2e636f6d2f696f732d61642d73646b2f616464696e672d6672616d65776f726b732e706e67)
* for Android
  * Android's target SDK 27 is supported
  * Error.nativeStackAndroid / expose a _nativeStackAndroid_ property to promises rejected with an Exception/Throwable - making native error stacks available inside Javascript

###v0.57.0~v0.57.8

####v0.57.0
* includes [599 commits by 73 different contributors](https://github.com/facebook/react-native/compare/0.56-stable...0.57-stable)!
* Accessibility APIs now support accessibility hints
* On iOS, WKWebView can now be used within the WebView component
* Android tooling has been updated
  * **SDK 27**+, **gradle 4.4**+, and support library 27+
* Support **Babel 7(Typescript)** stable landed
* Flow, React, and related packages have also been updated
  * Upgrade to **react-devtools@3.3.4**
* **WebView** will be moved to its own repo at react-native-community/react-native-webview.
  * 意即未來不再內建WebView元件，需安裝react-native-webview
* **NavigatorIOS** 即將捨棄(v0.58已捨棄)
* 從舊版(v0.56)更新作法
  * Change the _babel-preset_ dependency from "babel-preset-react-native": "^5", to **"metro-react-native-babel-preset": "^0.45.0"**
  * change the _.babelrc_ configuration
```json
{
  "presets": ["module:metro-react-native-babel-preset"]
}
```

##Expo更新注意事項

## Expo v30.0.0重點

 - Upcoming release will drop support for iOS 9 and Android 4.4
   - **最後一個ExpoSDK支援舊版iOS與Android**
 - More ExpoKit modules available
    * expo-print
    * expo-media-library
    * expo-analytics-segment
    * expo-payments-stripe
    * expo-contacts
    * expo-local-authentication
    * expo-location
    * expo-ads-admob
    * expo-font
    * expo-barcode-scanner
 - Expo CLI 2.0 replaces exp and XDE
   - includes both the expo command-line tool and Expo Dev Tools, a web based UI for setting up devices, viewing logs and more.

## Expo v29.0.0重點
- 開始expo-cli beta(取代原本使用XDE)
  - run `npm i -g（yarn global add） expo-cli` for a good time.
- 改善雲端測試環境Snack
  - 提出新的開發IDE環境號稱比vscode體驗更好([Monaco](https://github.com/Microsoft/monaco-editor))
- Push notifications reliability
  - [includes an asynchronous push receipt API, requiring FCM for new Android apps, and deprecating the legacy server-side API](https://blog.expo.io/upcoming-changes-to-the-expo-push-notification-service-summer-2018-254030da786b)
  - **push receipts** with the aforementioned IDs will be available from the Expo server. These push receipts contain success or error information with regard to Apple and Google’s push notification services, including **important information such as whether to stop sending notifications** to an inactive device.
- Added SMS API
  - provides access to the system’s UI/app for sending SMS messages. On `iOS`, this uses **MFMessageComposeViewController**, and **Intent.ACTION_SENDTO** on `Android`.
- 程式載入畫面
  - Before SDK 29, the only way to control the visibility of the splash screen was to use the AppLoading component
  - In SDK 29 you can now use SplashScreen to imperatively control visibility with the following two functions:SplashScreen.preventAutoHide() and SplashScreen.hide(). You can use this to build a splash screen that transitions smoothly and interestingly into your app
- 對Contacts API重大更新（[詳細直接看blog內容](https://blog.expo.io/expo-sdk-v29-0-0-is-now-available-f001d77fadf)）

## Expo v28.0.0重點
- 針對發布Android應用程式變更項目
  - Android targetSdkVersion updated to 26 and build tools updated to the latest version
    - This targetSdkVersion “will be required for new apps in August 2018, and for updates to existing apps in November 2018.”([Android Developers Blog.](https://android-developers.googleblog.com/2017/12/improving-app-security-and-performance.html))
  - Support for Android notification channels
  - Support for Android adaptive launcher icons
    - launcher icons are now configurable with the **android.adaptiveIcon property** in `app.json
- iOS部分
  - Added **StoreReview API** on iOS
  - Camera improvements
  - Major additions to Expo.AR (ARKit API)

## Expo v27.0.0重點
- 開始支援bundled assets in ExpoKit
  - with `exp detach` now support bundling your assets inside your native app archive so that **your app can launch with no internet**.
- 新的Expo支援api
  - New MediaLibrary API
  - New Haptic API
  - **支援Firebase Cloud Messaging (FCM)**
  - Assorted improvements to Audio/Video and ImageManipulator

## Expo v26.0.0重點
- The iOS client will no longer be able to open projects published by other Expo users.
- Improved iOS standalone app startup time
- Full control over updating apps over-the-air (OTA)
  - 修正應用程式版本同步機制
  - Synchronously updating on startup gives you a nice guarantee: your users will always be using the newest version of your app.
  - But this comes at a cost: app startup time will be significantly slower on poor network connections and when downloading new updates.
  - setting **updates.fallbackToCacheTimeout to 0** in `app.json` in order to always download updates in the background.