在Androoid平台區分非常多版本、裝置尺寸、SDK與API Level

#Android API Level與sdk版本對照表

API等級 | Android SDK版本 | 代號
---------|----------|---------
API等級1 |  Android 1.0
API等級2 |  Android 1.1 |  Petit Four 花式小蛋糕
API等級3 |  Android 1.5 |  Cupcake 紙杯蛋糕
API等級4 |  Android 1.6 |  Donut 甜甜圈
API等級5 |  Android 2.0 |  Éclair  松餅
API等級6 |  Android 2.0.1 |  Éclair 松餅
API等級7 |  Android 2.1 |  Éclair  松餅
API等級8 |  Android 2.2 - 2.2.3 |  Froyo 凍酸奶
API等級9 |  Android 2.3 - 2.3.2 |  Gingerbread 薑餅
API等級10 |   Android 2.3.3-2.3.7 |  Gingerbread 薑餅
API等級11 |   Android 3.0 |  Honeycomb 蜂巢
API等級12 |   Android 3.1 |  Honeycomb 蜂巢
API等級13 |   Android 3.2 |  Honeycomb 蜂巢
API等級14 |   Android 4.0 - 4.0.2 |  Ice Cream Sandwich 冰淇淋三明治
API等級15  |  Android 4.0.3 - 4.0.4 |  Ice Cream Sandwich 冰淇淋三明治
API等級16 |   Android 4.1 | Jelly Bean 糖豆
API等級17 |   Android 4.2 | Jelly Bean 糖豆
API等級18 |   Android 4.3 | Jelly Bean 糖豆
API等級19 |   Android 4.4 | KitKat 奇巧巧克力棒
API等級20 |   Android 4.4W | KitKat with wearable extensions 奇巧巧克力棒
API等級21 | Android 5.0-5.0.2 Lollipop | 棒棒糖
